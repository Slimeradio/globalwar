﻿using System.Collections.Generic;
using ANE.A;
using Logic;
using UnityEngine;

namespace DefaultNamespace
{
    public class Obstacle
    {
        public List<Vector2> Points = new List<Vector2>();
            
        public double leastAngle = double.MaxValue;
        public double mostAngle = double.MinValue;
        public int leastAngleIndex = 0;
        public int mostAngleIndex = 1;

        public Vector2 A => Points[leastAngleIndex];
        public Vector2 B => Points[mostAngleIndex];
            
        public void CalculateFor(Vector2 origin)
        {
            leastAngle = double.MaxValue;
            mostAngle = double.MinValue;
            leastAngleIndex = -1;
            mostAngleIndex = -1;
                
            for (int i = 0; i < Points.Count; i++)
            {
                var angle = origin.GetAngle2(Points[i]);
                if (angle > mostAngle)
                {
                    mostAngleIndex = i;
                    mostAngle = angle;
                }
                if (angle < leastAngle)
                {
                    leastAngleIndex = i;
                    leastAngle = angle;
                }
            }
        }
    }
    
    public class VisibilityCalculator
    {
        public static VisibilityCalculator Instance = new VisibilityCalculator();
        private int MaxRange = 10;

        private const float v1 = 0.32f;
        private const float v2 = 0.54f;
        private const float v3 = 0.64f;
        
        public static List<Vector2> HexOffsets = new List<Vector2>()
        {
            new Vector2(-v1, v2),
            new Vector2(v1, v2),
            new Vector2(v3, 0),
            new Vector2(v1, -v2),
            new Vector2(-v1, -v2),
            new Vector2(-v3, 0)
        };

        public List<KeyValuePair<Vector2, Cell>> AllCells = new List<KeyValuePair<Vector2, Cell>>();
        public List<Obstacle> Obstacles = new List<Obstacle>();
        
        public List<Cell> NotVisibleCells = new List<Cell>();
        public List<Cell> VisibleCells = new List<Cell>();
        
        public List<Cell> Calculate(Map map, P at)
        {
            NotVisibleCells = new List<Cell>();
            VisibleCells = new List<Cell>();
            
            Prepare(map, at);
            var positionFx = (Vector2)GameSettings.Instance.PositionFx(at.x, at.y);
            Logic(AllCells, Obstacles, positionFx, NotVisibleCells, VisibleCells);
            return NotVisibleCells;
        }

        public void Prepare(Map map, P at)
        {
            Obstacles.Clear();
            AllCells.Clear();
            
            var cells = map.GetCellsInRadius(MaxRange, at);
            cells.Remove(map[at]);
            
            foreach (var cell in cells)
            {
                var positionFx = (Vector2)GameSettings.Instance.PositionFx(cell);
                AllCells.Add(new KeyValuePair<Vector2, Cell>(positionFx, cell));
                
                if (cell.Obstacle != null)
                {
                    //var obs = new Obstacle();
                    //obs.Points.Add(positionFx + HexOffsets[0]);
                    //obs.Points.Add(positionFx + HexOffsets[3]);
                    //Obstacles.Add(obs);
                    //
                    //var obs2 = new Obstacle();
                    //obs2.Points.Add(positionFx + HexOffsets[1]);
                    //obs2.Points.Add(positionFx + HexOffsets[4]);
                    //Obstacles.Add(obs2);
                    //
                    //var obs3 = new Obstacle();
                    //obs3.Points.Add(positionFx + HexOffsets[2]);
                    //obs3.Points.Add(positionFx + HexOffsets[5]);
                    //Obstacles.Add(obs3);
                    
                    Obstacles.Add(cell.Obstacle);
                }
            }
        }
        
        public void Logic<T>(List<KeyValuePair<Vector2, T>> tests, List<Obstacle> obstacle, Vector2 origin, List<T> notVisible, List<T> visible)
        {
            foreach (var obs in obstacle)
            {
                obs.CalculateFor(origin);
            }

            foreach (var v in tests)
            {
                bool not = true;
                var angle = origin.GetAngle2(v.Key);
                
                foreach (var ob in obstacle)
                {
                    if (ob.leastAngle <= angle && angle <= ob.mostAngle)
                    {
                        if (v.Key.IsBetween2Lines(origin, ob.A, ob.B))
                        {
                            if (Geom.AreLinesIntersecting(origin, v.Key, ob.A, ob.B, true))
                            {
                                notVisible.Add(v.Value);
                                not = false;
                                break;
                            }
                        }
                    }
                }

                if (not)
                {
                    visible.Add(v.Value);
                }
            }
        }
    }
}