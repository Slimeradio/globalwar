﻿using System;
using System.Collections.Generic;
using System.Text;
using ANE.A;

namespace DefaultNamespace
{

    public class MapGeneratorVariantsDefinition : BaseDefinition
    {
        public List<MapGeneratorVariantDefinition> Variants;
    }
    
    public class MapGeneratorVariantDefinition
    {
        public string ID = "unknown";
        public string Cells;
        public string AllowedLeft;
        public string AllowedTop;
        public string AllowedRight;
        public string AllowedBottom;
    }
    
    public class MapGeneratorVariant
    {
        public const int Width = 6; 
        public const int Height = 6; 
        public string ID;
        public int[,] Cells = new int[Width, Height];
        public HashSet<string> AllowedLeft = new HashSet<string>();
        public HashSet<string> AllowedTop = new HashSet<string>();
        public HashSet<string> AllowedRight = new HashSet<string>();
        public HashSet<string> AllowedBottom = new HashSet<string>();

        public MapGeneratorVariantDefinition ToDefinition()
        {
            var def = new MapGeneratorVariantDefinition();
            def.ID = ID;
            def.AllowedLeft = string.Join(" ", AllowedLeft);
            def.AllowedTop = string.Join(" ", AllowedTop);
            def.AllowedRight = string.Join(" ", AllowedRight);
            def.AllowedBottom = string.Join(" ", AllowedBottom);

            var s = new StringBuilder();
            for (int y = 0; y < Cells.GetLength(1); y++)
            {
                s.Append("\r\n");
                for (int x = 0; x < Cells.GetLength(0); x++)
                {
                    s.Append(Cells[x,y]);
                    s.Append(" ");
                }
            }
            def.Cells = s.ToString();

            return def;
        }

        public void FromDefinition(MapGeneratorVariantDefinition def)
        {
            ID = def.ID;
            AllowedLeft = def.AllowedLeft.Split(" ").ToSet();
            AllowedTop = def.AllowedTop.Split(" ").ToSet();
            AllowedRight = def.AllowedRight.Split(" ").ToSet();
            AllowedBottom = def.AllowedBottom.Split(" ").ToSet();
            Cells = new int[Width,Height];
            
            var cells = def.Cells.ToInts(" ", "\r\n");
            for (int i = 0; i < cells.Count; i++)
            {
                int x = i % Width;
                int y = i / Width;

                Cells[x, y] = cells[i];
            }
        }
    }
    
    public class MapGenerator
    {
        private int Seed = 0;
        private Random random = new Random();
        private int chunkSize = 16;
        
        public void Generate(Map map, int x, int y)
        {
            var originX = x;
            var originY = y;
            
            int xx = x / chunkSize + (x < 0 ? (x % chunkSize == 0 ? 0 : -1)  : 0);
            int yy = y / chunkSize + (y < 0 ? (y % chunkSize == 0 ? 0 : -1)  : 0);
            
            int cellSeed;
            unchecked
            {
                cellSeed = xx * 10000 + yy;
            }
            random = new Random(cellSeed);

            var startX = xx * chunkSize;
            var endX = (xx + 1) * chunkSize;
            var startY = yy * chunkSize;
            var endY = (yy + 1) * chunkSize;

            for (x = startX; x < endX; x++)
            {
                for (y = startY; y < endY; y++)
                {
                    var cell = new Cell(x, y, random.Next(0, 30));
                    map.AddCell(x, y, cell);
                }
            }

            if (map.GetCell(new P(originX, originY), false) == null)
            {
                var wtf = 0;
                Generate(map, originX, originY);
            }
        }
    }
}