﻿namespace DefaultNamespace
{
    public interface IMapObject
    {
        Cell Cell { get; }
        void OnAddedToMap(Cell cell);
        Player GetPlayer();
    }
    
    public interface IAliveMapObject : IMapObject
    {
        bool IsAlive { get; }
    }
}