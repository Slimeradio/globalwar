using System;
using System.Collections.Generic;
using System.Linq;
using ANE.A;
using DefaultNamespace;
using UnityEngine;

public class Map
{
    public static Map Instance;
    public float DistanceBetweenCells = 100;
    //private Cell[,] Cells;
    private Dictionary<P, Cell> Celld = new Dictionary<P, Cell>();
    public Dictionary<Cell, List<Unit>> CellToUnit = new Dictionary<Cell, List<Unit>>();
    public Dictionary<Cell, List<Building>> CellToBuilding = new Dictionary<Cell, List<Building>>();
    private MapGenerator Generator = new MapGenerator();
    public List<Player> Players = new List<Player>();
    public event Action<IMapObject, Cell, Cell> OnMoveGlobal;
    public Cell this[P p]
    {
        get => GetCell(p);
    }
    
    public Cell this[int x, int y]
    {
        get => GetCell(new P(x, y));
    }
    
    public Cell GetCell(P p, bool generate = true)
    {
        var cell = Celld.GetOr(p, null);
        if (cell == null && generate)
        {
            Generator.Generate(this, p.x, p.y);
            cell = Celld.GetOr(p, null);
        }

        return cell;
    }

    public void Add(IMapObject unit, Cell cell)
    {
        unit.OnAddedToMap(cell);
    }
    

    public Map()
    {
        Instance = this;
    }

    public Map Init()
    {
        var m = new MapGeneratorVariantsDefinition();
        m.Variants = new List<MapGeneratorVariantDefinition>();
        var v1 = new MapGeneratorVariant().ToDefinition();
        m.Variants.Add(v1);
        R.Save("Assets/Resources/MapGenerators.xml", m);
        
        Player p1 = new Player { name = "p1", Color = Color.blue };
        Player p2 = new Player { name = "p2", Color = Color.green };
        
        Players.Add(p1);
        Players.Add(p2);

        var c = R.Get<UnitDefinition>("Default");
        var mine = R.Get<EcoProducerDefinition>("Mine");
        var farm = R.Get<EcoProducerDefinition>("Farm");
        var oilRig = R.Get<EcoProducerDefinition>("OilRig");
        
        Builders.BuildUnit(c, this[0, 0], p1);
        Builders.BuildUnit(c, this[2, 2], p2);

        Builders.BuildBuilding(mine, this[5, 2], null);
        Builders.BuildBuilding(farm, this[3, 6], null);
        Builders.BuildBuilding(oilRig, this[6, 6], null);

        return this;
    }


    private Dictionary<int, List<P>> RangesEven = new Dictionary<int, List<P>>();
    private Dictionary<int, List<P>> RangesOdd = new Dictionary<int, List<P>>();
    private HashSet<P> originalEvenX = new HashSet<P>
    {
        
        new P(0, -1),
        
        new P(-1, -1),
        new P(1, -1),
        
        //new P(0, 0),
        
        new P(-1, 0),
        new P(1, 0),
        
        new P(0, 1)
    };
    
    private HashSet<P> originalOddX = new HashSet<P>
    {
        new P(0, -1),
        
        new P(-1, 0),
        new P(1, 0),
        
        //new P(0, 0),

        new P(-1, 1),
        new P(1, 1),
        
        new P(0, 1)
    };


    private void Generate(HashSet<P> old, HashSet<P> neww, int baseX, int times)
    {
        foreach (var p in old)
        {

            var tx = baseX + p.x;

            var original = tx % 2 == 0 ? originalEvenX : originalOddX;
            
            foreach (var p1 in original)
            {
                neww.Add(p + p1);
            }
        }

        if (times > 0)
        {
            Generate(neww, old, baseX, times - 1);
        }
    }

    public List<P> GetRadiusBorder(int definitionRange, int atX)
    {
        var radius1 = GetRadius(definitionRange, atX);
        var radius2 = GetRadius(definitionRange-1, atX);

        foreach (var p in radius2)
        {
            radius1.Remove(p);
        }

        return radius1;
    }
    
    public List<P> GetRadius(int definitionRange, int atX)
    {
        HashSet<P> old = new HashSet<P> {new P(0, 0)};
        HashSet<P> neww = new HashSet<P>();
        
        Generate(old, neww, atX, definitionRange);

        List<P> p = (old.Count > neww.Count ? old : neww).ToList();

        //Ranges[definitionRange] = p;
        
        return p;
        //Ranges.GetOr()   
    }
    
    
    public List<Cell> GetCellsInRadius(int radius, Cell at)
    {
        return GetCellsInRadius(radius, at.X, at.Y);
    }
    
    public List<Cell> GetCellsInRadius(int radius, P at)
    {
        return GetCellsInRadius(radius, at.x, at.y);
    }
    
    public List<Cell> GetCellsInRadius(int definitionRange, int atX, int atY)
    {
        var range = GetRadius(definitionRange, atX);
        var list = new List<Cell>();
        foreach (var r in range)
        {
            var c = this[r.x + atX, r.y + atY];
            if (c != null)
            {
                list.Add(c);
            }
        }

        return list;
    }

    public void GetInRadius(Cell origin, int range, Action<IMapObject> filter)
    {
        var radius = GetRadius(range, origin.X);
        foreach (var p in radius)
        {
            var cell = this[p.x + origin.X, p.y + origin.Y];
            if (cell == null) continue;
            
            var units = CellToUnit.GetOr(cell, null);
            if (units == null) continue;

            foreach (var f in units)
            {
                filter.Invoke(f);
            }
        }
    }

    

    public bool IsInRange(Cell where, Cell cell, int range)
    {
        var r = GetRadius(range, cell.X);
        foreach (var p in r)
        {
            var c = this[p.x + cell.X, p.y + cell.Y];
            if (c == where)
            {
                return true;
            }
        }

        return false;
    }

    public void Unregister(Cell cell, Unit unit)
    {
        CellToUnit.RemoveAndKey(cell, unit);
        cell.TriggerUnitsChanged(unit, false);
    }
    
    public void Register(Cell cell, Unit unit)
    {
        CellToUnit.GetOrNew(cell).Add(unit);
        cell.TriggerUnitsChanged(unit, true);
    }
    
    public void Move(Cell from, Cell to, Unit unit)
    {
        if (from != null)
        {
            CellToUnit.RemoveAndKey(from, unit);
            from.TriggerUnitsChanged(unit, false);
        }

        if (to != null)
        {
            CellToUnit.GetOrNew(to).Add(unit);
            to.TriggerUnitsChanged(unit, true);
        }
        
        OnMoveGlobal?.Invoke(unit, from, to);
    }
    
    public void Unregister(Cell cell, Building building)
    {
        CellToBuilding.RemoveAndKey(cell, building);
    }
    
    public void Register(Cell cell, Building building)
    {
        CellToBuilding.GetOrNew(cell).Add(building);
    }

    public void AddCell(int x, int y, Cell cell)
    {
        Celld[new P(x, y)] = cell;
    }
    
    
    public List<P> FindPath(Cell from, Cell to, Unit unit)
    {
        return FindPath(from.X, from.Y, to.X, to.Y, unit);
    } 
        
    public List<P> FindPath(int fromX, int fromY, int toX, int toY, Unit unit)
    {

        int extra = 8;
        var left = new P (Math.Min(fromX, toX) - extra, Math.Min(fromY, toY) - extra);
        var right = new P (Math.Max(fromX, toX) + extra, Math.Max(fromY, toY) + extra);
        var w = right.x - left.x; 
        var h = right.y - left.y; 
            
        HexagonFinder hf = new HexagonFinder(this, unit, w, h, left);
        GlyphFinder gf = new GlyphFinder();
        gf.Init(hf, hf);
        gf.SetDestination(toX - left.x, toY - left.x);
        var dst = gf.StartWave(new P(fromX - left.x, fromY - left.x));


        //for (int x = gf.minX; x < gf.maxX; x++)
        //{
        //    for (int y = gf.minX; y < gf.maxX; y++)
        //    {
        //        var v = gf.GetValue(x, y);
        //        var presenter = CellPresenters.GetOr(Map[x, y], null);
        //        if (presenter != null)
        //        {
        //            presenter.Text.text = "" + v;
        //        }
        //    } 
        //}
            
        if (dst != null)
        {
            var list = gf.FindWayBack2(dst.Value.x, dst.Value.y, hf);
            list?.Reverse();

            for (int i = 0; i < list.Count; i++)
            {
                list[i] += left;
            }
            return list;
        }

        return null;
    } 
}


