﻿using System;
using System.Collections.Generic;
using ANE.A;
using UnityEngine;

namespace DefaultNamespace
{
    class CellsDefinition
    {
        public static CellsDefinition Instance = new CellsDefinition();
        
        public CellDefinition[] Cells = new CellDefinition[]
        {
            new CellDefinition(true),//0+
            new CellDefinition(false),//1+
            new CellDefinition(true),//2+
            new CellDefinition(true),//3+
            new CellDefinition(true),//4+
            new CellDefinition(true),//5+
            new CellDefinition(false),//6+
            new CellDefinition(false),//7+
            new CellDefinition(false),//8+
            new CellDefinition(false),//9+
            new CellDefinition(false),//10+
            new CellDefinition(false),//11+
            new CellDefinition(true),//12+
            new CellDefinition(true),//13+
            new CellDefinition(false),//14+
            new CellDefinition(false),//15+
            new CellDefinition(false),//16+
            new CellDefinition(false),//17+
            new CellDefinition(false),//18+
            new CellDefinition(false),//19+
            new CellDefinition(true),//20+
            new CellDefinition(false),//21+
            new CellDefinition(true),//22+
            new CellDefinition(true),//23+
            new CellDefinition(false),//24+
            new CellDefinition(false),//25+
            new CellDefinition(false),//26+
            new CellDefinition(false),//27+
            new CellDefinition(false),//28+
            new CellDefinition(true),//29+
        };
    }

    struct CellDefinition
    {
        public bool Visible;

        public CellDefinition(bool b)
        {
            Visible = b;
        }
    }
    
    public class Cell
    {
        
        
        public int X;
        public int Y;
        public int Type;
        public Vector2 Position;
        public bool BlocksVisibility => !CellsDefinition.Instance.Cells[Type].Visible;
        public Obstacle Obstacle = null;
        public event Action<Cell, Unit, bool> OnUnitsChanged;
        
        private List<Cell> NotVisibleCells = null;
        
        public List<Cell> GetNotVisibleCells()
        {
            if (NotVisibleCells == null)
            {
                NotVisibleCells = VisibilityCalculator.Instance.Calculate(Map.Instance, new P(X, Y));
            }
                
            return NotVisibleCells;
        }
        
        public Cell(int x, int y, int type)
        {
            X = x;
            Y = y;
            Type = type;

            Position = GameSettings.Instance.PositionFx(this);

            if (BlocksVisibility)
            {
                Obstacle = new Obstacle();
                foreach (var offset in VisibilityCalculator.HexOffsets)
                {
                    Obstacle.Points.Add(Position + offset);
                }
            }
        }

        

        public void TriggerUnitsChanged(Unit unit, bool added)
        {
            OnUnitsChanged?.Invoke(this, unit, added);
        }
    }
}