﻿using System;
using System.Collections.Generic;
using DefaultNamespace;

namespace ANE.A
{
    public class HexagonFinder : WaveGenerator, PointChecker
    {
        private Map map;
        private Unit unit;
        private int width;
        private int height;
        private P mapOffset;

        public HexagonFinder(Map map, Unit unit, int width, int height, P mapOffset)
        {
            this.map = map;
            this.unit = unit;
            this.width = width;
            this.height = height;
            this.mapOffset = mapOffset;
            
            if (map == null)
            {
                throw new ArgumentException("Map is null");
            }
        }


        public void Startwave(GlyphFinder finder, int x, int y)
        {
            
        }

        public bool CanMove(P from, P to)
        {
            return true;
        }

        public int GetWidth()
        {
            return width;
        }

        public int GetHeight()
        {
            return height;
        }
        
        public void Generate(GlyphFinder finder, P from, int frame)
        {
            Test(finder, from,  0, -1, frame);
            Test(finder, from,  0, 1, frame);
            
            if (from.x % 2 == 1)
            {
                Test(finder, from,  -1, 0, frame);
                Test(finder, from,  1, 0, frame);
                Test(finder, from,  -1, 1, frame);
                Test(finder, from,  1, 1, frame);
            }
            else
            {
                Test(finder, from,  -1, -1, frame);
                Test(finder, from,  1, -1, frame);
                Test(finder, from,  -1, 0, frame);
                Test(finder, from,  1, 0, frame);
            }
            
        }

        public void GeneratePossibleMoves(List<P> list, P from)
        {
            list.Add(new P( from.x + 0, from.y + -1));
            list.Add(new P( from.x + 0, from.y + 1));
            
            if (from.x % 2 == 1)
            {
              list.Add(new P( from.x + -1, from.y + 0));
              list.Add(new P( from.x + 1, from.y + 0));
              list.Add(new P( from.x + -1, from.y + 1));
              list.Add(new P( from.x + 1, from.y + 1));
            }
            else
            {
              
              list.Add(new P( from.x + -1, from.y + -1));
              list.Add(new P( from.x + 1, from.y + -1));
              list.Add(new P( from.x + -1, from.y + 0));
              list.Add(new P( from.x + 1, from.y + 0));
            }
        }


        private void Test(GlyphFinder finder, P from, int dx, int dy,  int frame)
        {
            var dst = new P(from.x + dx, from.y + dy);
            if (dst.x < 0) return;
            if (dst.y < 0) return;
            if (dst.x >= GetWidth()) return;
            if (dst.y >= GetHeight()) return;
            
            var cell = map[dst + mapOffset];
            var timeToMove = unit.Definition.PerSurfaceMovementSpeed[cell.Type];
            if (timeToMove > 0)
            {
                finder.Test(from, dx, dy, frame+timeToMove);    
            }
        }
    }
}