﻿using System;
using System.Collections.Generic;

namespace ANE.A
{

public interface PointChecker
{
    void Startwave(GlyphFinder finder, int x, int y);
    bool CanMove(P from, P to);
    int GetWidth();
    int GetHeight();
}

public interface WaveGenerator
{
    void Generate(GlyphFinder finder, P from, int frame);
    void GeneratePossibleMoves(List<P> ways, P point);
}

public class GlyphFinder : DestinationAlgorythm
{
    public int minX = 999999;
    public int minY = 999999;
    
    public int maxX, maxY;

    public Recta bounds;
    public PointChecker canMoveFunc;
    public WaveGenerator generator;

    public void ReInit(Recta? bounds=null)
    {
        Init (canMoveFunc.GetWidth(), canMoveFunc.GetHeight());
        
        if (bounds == null)
        {
            this.bounds = new Recta(0,0, canMoveFunc.GetWidth(), canMoveFunc.GetHeight());
        }
        else
        {
            this.bounds = bounds.Value;
        }
    }
    
    public void Init (PointChecker canMoveFunc, WaveGenerator generator = null, Recta? bounds=null)
    {
        this.generator = generator;
        this.canMoveFunc = canMoveFunc;

        Init (canMoveFunc.GetWidth(), canMoveFunc.GetHeight());
        
        if (bounds == null)
        {
            this.bounds = new Recta(0,0, canMoveFunc.GetWidth(), canMoveFunc.GetHeight());
        }
        else
        {
            this.bounds = bounds.Value;
        }
    }

    public override P? StartWave(P point)
    {
        point = point.Copy();
        canMoveFunc.Startwave(this, point.x, point.y);
        return base.StartWave(point);
    }

    protected override void GenerateWave(P p, int frame)
    {
        generator.Generate(this, p, frame);
    }

    public void Test(P p, int dx, int dy, int wouldBeAtFrame)
    {
        var to = new P(p.x + dx,p.y + dy);
        if (to.y >= bounds.Top && to.y < bounds.Bottom && to.x >= bounds.Left && to.x < bounds.Right)
        {
            if (!CanVisit(ref p, ref to, wouldBeAtFrame))
            {
                return;
            }
            if (path[to.x, to.y] == DESTINATION)
            {
                AddPoint(to, wouldBeAtFrame);
                throw new ReachedDestinationException(new P(to.x, to.y));
            }

            AddPoint(to, wouldBeAtFrame);
        }
    }

    public override bool CanMove(ref P from, ref P to)
    {
        return canMoveFunc.CanMove(from, to);
    }

    protected override void Visit(P p, int wouldBeAtFrame)
    {
        base.Visit (p, wouldBeAtFrame);


        minX = Math.Min(minX, p.x);
        maxX = Math.Max(maxX, p.x);
        minY = Math.Min(minY, p.y);
        maxY = Math.Max(maxY, p.y);
    }

    public Recta GetRectangle()
    {
        return new Recta(minX, minY, maxX - minX, maxY - minY);
    }
    
    public List<P> GetBorder(int dx = 1, int dy = 1)
    {
        List<P> borders = new List<P>();
        for (int x = minX; x <= maxX; x++)
        {
            for (int y = minY; y <= maxY; y++)
            {
                if (GetValue(x, y) >= 0 && IsBorder (x, y, dx, dy))
                {
                    borders.Add(new P(x,y));                
                }
            }
        }

        return borders;
    }
    
    public List<P> GetAllPixels()
    {
        List<P> pixels = new List<P>();
        for (int x = minX; x <= maxX; x++)
        {
            for (int y = minY; y <= maxY; y++)
            {
                if (GetValue(x, y) >= 0)
                {
                    pixels.Add(new P(x,y));                
                }
            }
        }

        return pixels;
    }
    
    public bool IsBorder(int x, int y, int dx = 1, int dy = 1)
    {
        bool isB = false;
        For(x - dx, y - dy, x + dx, y + dy, (xx, yy) =>
        {
            isB |= GetValue(xx, yy) < 0;
        });
        return isB;
    }

    public bool IsEmpty()
    {
        return minX == 999999;
    }

    public void SetDestination(int x, int y)
    {
        path[x, y] = DESTINATION;
    }
}
}
