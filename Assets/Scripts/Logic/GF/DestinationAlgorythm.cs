﻿using System;
using System.Collections.Generic;
using DefaultNamespace;

namespace ANE.A
{
    public abstract class DestinationAlgorythm : WaveAlgorythm
    {
        private Dictionary<P, int> when = new Dictionary<P, int>();
        private Dictionary<int, List<P>> frames = new Dictionary<int, List<P>>();
        private int maxFrame = -1;

        public virtual void Init(int w, int h)
        {
            PrepareField(w, h);
            frames.Clear();

            when.Clear();
            maxFrame = -1;
        }

        protected override void AddPoint(P to, int wouldBeAtFrame)
        {
            maxFrame = Math.Max(wouldBeAtFrame, maxFrame);

            if (!when.TryGetValue(to, out w))
            {
                when[to] = wouldBeAtFrame;
                frames.GetOrNew(wouldBeAtFrame).Add(to);
                Visit(to, wouldBeAtFrame);
                return;
            }

            if (wouldBeAtFrame < w)
            {
                when[to] = wouldBeAtFrame;
                frames.GetOrNew(wouldBeAtFrame).Add(to);
                Visit(to, wouldBeAtFrame);
            }
        }

        protected override void DoLogic(P point)
        {
            AddPoint(point, 0);

            for (int i = 0; i <= maxFrame; i++)
            {
                var set = frames.GetOr(i, null);
                if (set == null)
                {
                    continue;
                }

                foreach (var p in set)
                {
                    GenerateWave(p, i);
                }
            }
        }
    }
}