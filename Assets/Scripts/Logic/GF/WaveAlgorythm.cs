﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ANE.A
{
    public class ReachedDestinationException : Exception
    {
        public P Destination;

        public ReachedDestinationException(P destination)
        {
            Destination = destination;
        }
    }

    

    

    public abstract class WaveAlgorythm
    {
        public const int DESTINATION = -3;
        public const int UNCALCULATED = -2;
        public const int UNKNOWN = -1;
        public const int VALID = 0;

        public int w, h;

        protected int[,] path; //same size as data;


        protected abstract void GenerateWave(P p, int frame);
        public abstract bool CanMove(ref P from, ref P to);

        public override string ToString()
        {
            var sb = new StringBuilder();
            for (int x = 0; x < path.GetLength(0); x++)
            {
                for (int y = 0; y < path.GetLength(1); y++)
                {
                    sb.Append(path[x, y].ToString("000")).Append(" ");
                }

                sb.AppendLine();
            }

            return sb.ToString();
        }

        public int GetValue(int x, int y)
        {
            return path[x, y];
        }

        public int GetValueOrInvalid(int x, int y)
        {
            if (x < path.GetLength(0) && y < path.GetLength(1) && x >= 0 && y >= 0)
            {
                return path[x, y];
            }

            return UNCALCULATED;
        }

        public void PrepareField(int w, int h)
        {
            this.w = w;
            this.h = h;

            if (path == null || path.GetLength(0) != w && path.GetLength(1) != h)
            {
                path = new int[w, h];
            }

            for (var x = 0; x < w; x++)
            {
                for (var y = 0; y < h; y++)
                {
                    path[x, y] = UNCALCULATED;
                }
            }
        }

        public bool CanVisit(ref P from, ref P to, int wouldBeAtFrame)
        {
            return (path[to.x, to.y] < VALID || wouldBeAtFrame < path[to.x, to.y]);
        }


        protected abstract void AddPoint(P to, int wouldBeAtFrame);
        protected abstract void DoLogic(P point);


        public virtual P? StartWave(P point)
        {
            try
            {
                if (!CanMove(ref point, ref point))
                {
                    return null;
                }


                DoLogic(point);

                return null;
            }
            catch (ReachedDestinationException e)
            {
                return e.Destination;
            }
        }

        protected virtual void Visit(P p, int wouldBeAtFrame)
        {
            path[p.x, p.y] = wouldBeAtFrame;
        }

        //=============== SUGAR ===============

        public List<P> FindWayBack(int x, int y, List<P> list = null)
        {
            if (list == null) list = new List<P>();
            //THIS IS NOT CORRECT, AS IT USING UNKNOWN 8-WAY picker.

            var v = GetValue(x, y);
            if (v < 0) return null;
            if (v == 0) return list;

            P best = new P(x, y);


            For(x - 1, y - 1, x + 1, y + 1, (xx, yy) =>
            {
                var vv = GetValue(xx, yy);
                if (vv < v && vv >= VALID)
                {
                    best.x = xx;
                    best.y = yy;
                    v = vv;
                }
            });

            if (best.x == x && best.y == y)
                throw new Exception("WTF?");

            list.Add(best);
            return FindWayBack(best.x, best.y, list);
        }

        public List<P> FindWayBack2(int x, int y, WaveGenerator waveAlgorythm, List<P> list = null, List<P> ways = null)
        {
            if (list == null) list = new List<P>();
            if (ways == null) ways = new List<P>();
            //THIS IS NOT CORRECT, AS IT USING UNKNOWN 8-WAY picker.
            list.Add(new P(x, y));
            
            var v = GetValueOrInvalid(x, y);
            if (v < 0) return null;
            if (v == 0) return list;

            var possibles = new List<P>();


            waveAlgorythm.GeneratePossibleMoves(ways, new P(x, y));
            if (ways.Count == 0)
            {
                waveAlgorythm.GeneratePossibleMoves(ways, new P(x, y));
            }
            foreach (var w in ways)
            {
                var vv = GetValueOrInvalid(w.x, w.y);
                if (vv <= v && vv >= VALID)
                {
                    if (vv < v)
                    {
                        possibles.Clear();
                    }

                    v = vv;

                    possibles.Add(w);
                }
            }
            
            ways.Clear();

            

            if (possibles.Count == 1 || list.Count < 2)
            {
                return FindWayBack2(possibles[0].x, possibles[0].y, waveAlgorythm, list, ways);
            }

            var p1 = list[list.Count - 1];
            var p2 = list[list.Count - 2];

            var dx = Math.Abs(Math.Sign(p1.x - p2.x));
            var dy = Math.Abs(Math.Sign(p1.x - p2.x));

            foreach (var possible in possibles)
            {
                var dx2 = Math.Abs(Math.Sign(possible.x - p1.x));
                var dy2 = Math.Abs(Math.Sign(possible.x - p1.x));

                if (dx2 == dx && dy2 == dy)
                {
                    return FindWayBack2(possibles[0].x, possibles[0].y, waveAlgorythm, list, ways);
                }
            }

            return FindWayBack2(possibles[0].x, possibles[0].y, waveAlgorythm, list, ways);
        }


        public List<P> FindWaysBack(int x, int y, List<P> list)
        {
            //THIS IS NOT CORRECT, AS IT USING UNKNOWN 8-WAY picker.

            var v = GetValue(x, y);
            if (v < 0) return null;
            if (v == 0) return list;

            var tmp = new List<P>();
            int bestV = v;
            For(x - 1, y - 1, x + 1, y + 1, (xx, yy) =>
            {
                var vv = GetValue(xx, yy);
                if (vv < VALID) return;

                if (vv < bestV)
                {
                    tmp.Clear();
                    bestV = vv;
                    tmp.Add(new P(xx, yy));
                }
                else if (vv == bestV && bestV != v)
                {
                    tmp.Add(new P(xx, yy));
                }
            });

            if (bestV == v)
                throw new Exception("WTF?");


            list.AddRange(tmp);

            foreach (var p in tmp)
            {
                FindWaysBack(p.x, p.y, list);
            }

            return list;
        }

        public void For(int minX, int minY, int maxX, int maxY, Action<int, int> a)
        {
            minX = Math.Max(0, minX);
            maxX = Math.Min(w - 1, maxX);
            minY = Math.Max(0, minY);
            maxY = Math.Min(h - 1, maxY);

            for (int xx = minX; xx <= maxX; xx++)
            {
                for (int yy = minY; yy <= maxY; yy++)
                {
                    a(xx, yy);
                }
            }
        }
    }
}