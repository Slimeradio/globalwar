﻿using System.Collections.Generic;

namespace ANE.A
{
    public abstract class Wave2Algorythm : WaveAlgorythm
    {
        private HashSet<P> wavegenerators = new HashSet<P>();
        private HashSet<P> nextWavegenerators = new HashSet<P>();

        public virtual void Init(int w, int h)
        {
            PrepareField(w, h);
            wavegenerators.Clear();
            nextWavegenerators.Clear();
        }

        protected override void AddPoint(P to, int wouldBeAtFrame)
        {
            nextWavegenerators.Add(to);
            Visit(to, wouldBeAtFrame); //Can be called more than 1 time
        }

        protected override void DoLogic(P point)
        {
            wavegenerators.Add(point);
            Visit(point, 0);


            while (wavegenerators.Count > 0)
            {
                foreach (var w in wavegenerators)
                {
                    GenerateWave(w, GetValue(w.x, w.y));
                }

                wavegenerators.Clear();

                var a = wavegenerators;
                var b = nextWavegenerators;
                nextWavegenerators = a;
                wavegenerators = b;
            }
        }
    }
}