﻿using System;
using System.Xml.Serialization;

namespace ANE.A
{
    public struct RectaF {
        [XmlAttribute]
        public float Left { get; set; }
        [XmlAttribute]
        public float Top { get; set; }
        [XmlAttribute]
        public float Right { get; set; }
        [XmlAttribute]
        public float Bottom { get; set; }

        public float Width => Right - Left;
        public float Height => Bottom - Top;
        public float CenterX => Width/2;
        public float CenterY => Height/2;

        public RectaF(float left, float top, float right, float bottom)
        {
            Left = left;
            Top = top;
            Right = right;
            Bottom = bottom;
        }


        public RectaF MinMax(RectaF other)
        {
            return new RectaF(
                Math.Min(Left, other.Left),
                Math.Min(Top, other.Top),
                Math.Max(Right, other.Right),
                Math.Max(Bottom, other.Bottom)
            );
        }

        public bool Contains(int x, int y)
        {
            return Left <= x && x <= Right && Top <= y && y <= Bottom;
        }
    }
    
    public struct Recta {
        [XmlAttribute]
        public int Left { get; set; }
        [XmlAttribute]
        public int Top { get; set; }
        [XmlAttribute]
        public int Right { get; set; }
        [XmlAttribute]
        public int Bottom { get; set; }

        public int Width => Right - Left;
        public int Height => Bottom - Top;
        public int CenterX => Width/2;
        public int CenterY => Height/2;
        public P LeftUp => new P(Left, Top);
        public P RightBottom => new P(Right, Bottom);

        public Recta(int left, int top, int right, int bottom)
        {
            Left = left;
            Top = top;
            Right = right;
            Bottom = bottom;
        }

        public bool Contains(P item1)
        {
            return Left <= item1.x && item1.x <= Right && Top <= item1.y && item1.y <= Bottom;
        }
        
        public bool Contains(int x, int y)
        {
            return Left <= x && x <= Right && Top <= y && y <= Bottom;
        }

        public Recta MinMax(Recta other)
        {
            return new Recta(
                Math.Min(Left, other.Left),
                Math.Min(Top, other.Top),
                Math.Max(Right, other.Right),
                Math.Max(Bottom, other.Bottom)
            );
        }

        public static void IterateOverBoth(Recta? old, Recta? neww, Action<int, int, bool> apply)
        {
            if (!old.HasValue && !neww.HasValue)
            {
                return;
            }
            
            Recta r = new Recta();
            if (!old.HasValue)
            {
                r = neww.Value;
                for (int x = r.Left; x <= r.Right; x++)
                {
                    for (int y = r.Top; y <= r.Bottom; y++)
                    {
                        apply.Invoke(x, y, true);
                    }
                }
                return;
            }
            
            if (!neww.HasValue)
            {
                r = old.Value;
                for (int x = r.Left; x <= r.Right; x++)
                {
                    for (int y = r.Top; y <= r.Bottom; y++)
                    {
                        apply.Invoke(x, y, true);
                    }
                }
                return;
            }

            
            var intersection = neww.Value.GetOverlap(old.Value);
            
            r = old.Value;
            for (int x = r.Left; x <= r.Right; x++)
            {
                for (int y = r.Top; y <= r.Bottom; y++)
                {
                    if (intersection == null || !intersection.Value.Contains(x, y))
                    {
                        apply.Invoke(x, y, false);
                    }
                }
            }
            
            r = neww.Value;
            for (int x = r.Left; x <= r.Right; x++)
            {
                for (int y = r.Top; y <= r.Bottom; y++)
                {
                    if (intersection == null || !intersection.Value.Contains(x, y))
                    {
                        apply.Invoke(x, y, true);
                    }
                }
            }
        }

        public void Iterate(Action<int,int, int, int> action)
        {
            for (int y = Top; y <= Bottom; y++)
            {
                for (int x = Left; x <= Right; x++)
                {
                    action(x, y, x-Left, y-Top);
                }   
            }
        }

        public bool Intersects(Recta other)
        {
            return 
                other.Right > Left && 
                other.Left < Right && 
                other.Bottom > Top && 
                other.Top < Bottom;
        }

        public Recta? GetOverlap(Recta b)
        {

            if (!Intersects(b)) return null;

            return new Recta(
                Math.Max(Left, b.Left),
                Math.Max(Top, b.Top),
                Math.Min(Right, b.Right),
                Math.Min(Bottom, b.Bottom)
                );
        }
        
    }
}