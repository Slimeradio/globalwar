﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Xml.Serialization;

namespace ANE.A
{
    [SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
    public struct P
    {
        [XmlAttribute("X")]
        public int x;
        
        [XmlAttribute("Y")]
        public int y;

    
        public P(P other)
        {
            x = other.x;
            y = other.y;
        }

        public P(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
        
        public static P operator +(P a, P b) => new P(a.x + b.x , a.y + b.y);
        public static P operator -(P a, P b) => new P(a.x - b.x , a.y - b.y);

        public double LengthSquared()
        {
            return x*x+y*y;
        }
        
        public double LengthSquared(P p)
        {
            var dx = x - p.x;
            var dy = y - p.y;
            return dx*dx+dy*dy;
        }

        public bool Equals(P other)
        {
            return x == other.x && y == other.y;
        }

        public override bool Equals(object obj)
        {
            return obj is P other && Equals(other);
        }
        
        public override int GetHashCode()
        {
            return y * 1024*16 + x;
        }

        public double Distance(P vKey)
        {
            int dx = x - vKey.x;
            int dy = y - vKey.y;
            return Math.Sqrt(dx * dx + dy * dy);
        }
        
        public double Distance(int xx, int yy)
        {
            int dx = x - xx;
            int dy = y - yy;
            return Math.Sqrt(dx * dx + dy * dy);
        }
        
        public static double Distance(int x1, int y1, int x2, int y2)
        {
            int dx = x1 - x2;
            int dy = y1 - y2;
            return Math.Sqrt(dx * dx + dy * dy);
        }

        public override string ToString()
        {
            return $"{x}:{y}";
        }

        public P Copy()
        {
            return new P(x, y);
        }

        

        public void InCircle(double r, Action<int, int> action, Func<int, int, bool> validateRange = null)
        { 
            for (int xx = x-(int)r-1; xx <= x+(int)r+1; xx++)
            {
                for (int yy = y-(int)r-1; yy <= y+(int)r+1; yy++)
                {
                    if (validateRange == null || validateRange(xx, yy))
                    {
                        if (Distance(xx, yy) <= r)
                        {
                            action(xx, yy);
                        }
                    }
                }
            }
        }
        
        public void InSquare(int r, Action<int, int> action, Func<int, int, bool> validateRange = null)
        {
            for (int xx = x-r; xx <= x+r; xx++)
            {
                for (int yy = y-r; yy <= y+r; yy++)
                {
                    if (validateRange == null || validateRange(xx, yy))
                    {
                        action(xx, yy);
                    }
                }
            }
        }

        public Recta ToRect(int i)
        {
            return new Recta
            {
                Left = x - i,
                Right = x + i,
                Top = y - i,
                Bottom = y + i,
            };
        }

        public void Min(P other)
        {
            x = Math.Min(other.x, x);
            y = Math.Min(other.y, y);
        }
    
        public void Max(P other)
        {
            x = Math.Max(other.x, x);
            y = Math.Max(other.y, y);
        }
    }
}