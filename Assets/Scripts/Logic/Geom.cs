﻿using System;
using UnityEngine;

namespace Logic
{
    public static class Geom
    {
        public static double GetAngle(this Vector2 a, Vector2 b)
        {
            return Math.Atan2(b.y - a.y, b.x - a.x);
        }
        
        public static bool IsBetween2Lines(this Vector2 test, Vector2 origin, Vector2 a, Vector2 b)
        {
            var sign1 = Geom.IsToLeftOrRight(origin, a, test);
            if (sign1 == 0)
            {
                return true;
            }
            var sign2 = Geom.IsToLeftOrRight(origin, b, test);
            if (sign2 == 0)
            {
                return true;
            }

            if (sign1 != sign2)
            {
                return true;
            }

            return false;
        }

        public static Vector3 Lerp(this Vector3 origin, Vector3 other, float amount)
        {
            var dx = other - origin;
            return origin + dx * amount;
        }
        
        public static int IsToLeftOrRight(Vector2 A, Vector2 B, Vector2 Test)
        {
            return Math.Sign((B.x - A.x) * (Test.y - A.y) - (B.y - A.y) * (Test.x - A.x));
        }
        
        //https://dirask.com/posts/C-NET-Math-Atan2-method-example-Dg0dmp
        public static double GetAngle2(this Vector2 a, Vector2 b)
        {
            var v= Math.Atan2(b.y - a.y, b.x - a.x);
            if (v < 0)
            {
                return 2*Math.PI + v;
            }

            return v;
        }
        
        public static bool AreLinesIntersecting(Vector2 l1_p1, Vector2 l1_p2, Vector2 l2_p1, Vector2 l2_p2, bool shouldIncludeEndPoints)
        {
            //To avoid floating point precision issues we can add a small value
            float epsilon = 0.00001f;

            bool isIntersecting = false;

            float denominator = (l2_p2.y - l2_p1.y) * (l1_p2.x - l1_p1.x) - (l2_p2.x - l2_p1.x) * (l1_p2.y - l1_p1.y);

            //Make sure the denominator is > 0, if not the lines are parallel
            if (denominator != 0f)
            {
                float u_a = ((l2_p2.x - l2_p1.x) * (l1_p1.y - l2_p1.y) - (l2_p2.y - l2_p1.y) * (l1_p1.x - l2_p1.x)) / denominator;
                float u_b = ((l1_p2.x - l1_p1.x) * (l1_p1.y - l2_p1.y) - (l1_p2.y - l1_p1.y) * (l1_p1.x - l2_p1.x)) / denominator;

                //Are the line segments intersecting if the end points are the same
                if (shouldIncludeEndPoints)
                {
                    //Is intersecting if u_a and u_b are between 0 and 1 or exactly 0 or 1
                    if (u_a >= 0f + epsilon && u_a <= 1f - epsilon && u_b >= 0f + epsilon && u_b <= 1f - epsilon)
                    {
                        isIntersecting = true;
                    }
                }
                else
                {
                    //Is intersecting if u_a and u_b are between 0 and 1
                    if (u_a > 0f + epsilon && u_a < 1f - epsilon && u_b > 0f + epsilon && u_b < 1f - epsilon)
                    {
                        isIntersecting = true;
                    }
                }
            }

            return isIntersecting;
        }
        
    }
}