﻿using System;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace;

namespace Logic
{
    public class AreaWatcher
    {
        public IMapObject Owner;
        public int WatchRadius = 0; //BaseDefinition.CaptureRadius
        public readonly List<Unit> UnitsNear = new List<Unit>();
        public readonly Dictionary<Player, int> UnitsCountByPlayer = new Dictionary<Player, int>();
        public event Action OnChanged;
        

        public AreaWatcher(IMapObject owner, int watchRadius)
        {
            Owner = owner;
            WatchRadius = watchRadius;
        }

        public void OnCellChanged(Cell old, Cell cell)
        {
            if (old != null)
            {
                var radius = Map.Instance.GetCellsInRadius(WatchRadius, old);
                foreach (var cc in radius)
                {
                    cc.OnUnitsChanged -= OnUnitsInCaptureRangeChanged;
                }
            }
            
            if (cell != null)
            {
                var radius = Map.Instance.GetCellsInRadius(WatchRadius, cell);
                foreach (var cc in radius)
                {
                    cc.OnUnitsChanged += OnUnitsInCaptureRangeChanged;
                }
            }
        }
        
        private void OnUnitsInCaptureRangeChanged(Cell cell, Unit u, bool added)
        {
            if (added)
            {
                UnitsNear.Add(u);
            }
            else
            {
                UnitsNear.Remove(u);
            }

            UnitsCountByPlayer.Clear();
            foreach (var unit in UnitsNear)
            {
                UnitsCountByPlayer.Sum(unit.Owner, 1);
            }

            OnChanged?.Invoke();
        }
    }
}