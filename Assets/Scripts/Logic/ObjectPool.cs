﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Logic
{
    public class ObjectPools<T, K>
    {
        private int max = 10;
        private List<T> data;
        private Func<T> creator;
        private Action<T> cleaner;
        private Action<T,K> apply;
        private Action<T> destroy;

        public ObjectPools(Func<T> creator, Action<T> cleaner = null, Action<T,K> apply = null, Action<T> destroy = null,int max = 10)
        {
            this.cleaner = cleaner;
            this.creator = creator;
            this.apply = apply;
            this.destroy = destroy;
            this.max = max;
            data = new List<T>(max);
        }

        public T Take(K k)
        {
            if (data.Count > 0)
            {
                var d = data.Last();
                data.RemoveAt(data.Count - 1);
                apply?.Invoke(d, k);
                return d;
            }

            var cc = creator.Invoke();
            apply?.Invoke(cc, k);
            return cc;
        }

        public void Put(T item)
        {
            if (data.Count < max)
            {
                data.Add(item);
                cleaner?.Invoke(item);
            }
            else
            {
                destroy?.Invoke(item);
            }
        }
    }
}