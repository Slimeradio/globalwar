﻿using System.Collections.Generic;
using System.Diagnostics;
using ANE.A;
using DebugHelpers;
using DefaultNamespace;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Logic
{
    public class GeomChecker : MonoBehaviour
    {
        private List<Obstacle> obstacles = new List<Obstacle>();
        private List<Vector2> testPoints = new List<Vector2>();
        private List<Vector2> visible = new List<Vector2>();
        private List<Vector2> notVisible = new List<Vector2>();

        private VisibilityCalculator VisibilityCalculator = new VisibilityCalculator();
        
        private void Awake()
        {
            //Random r = new Random(0);
            //LineRenderer = new LineDrawer();
            

            //for (float x = -7; x < 7; x+=0.5f)
            //{
            //    for (float y = -7; y < 7; y+=0.5f)
            //    {
            //        testPoints.Add(new Vector2(x, y));
            //        LineRenderers.Add(new LineDrawer());
            //    }
            //}
            //
            //for (int i = 0; i < 100; i++)
            //{
            //    var obstacle = new Obstacle();
            //    obstacles.Add(obstacle);
            //    var start = new Vector2(r.NextFloat(-5, 5), r.NextFloat(-5, 5));
            //    obstacle.Points.Add(start);
            //    obstacle.Points.Add(start + new Vector2(r.NextFloat(-1, 1), r.NextFloat(-1, 1)));
            //    //obstacle.Points.Add(new Vector2(r.NextFloat(-5, 5), r.NextFloat(-5, 5)));
            //    LineRenderers2.Add(new LineDrawer());
            //}
        }


        private void Update()
        {
            if (true) return;
            
            visible.Clear();
            notVisible.Clear();

            var st = new Stopwatch();
            st.Start();
            VisibilityCalculator.Calculate(Map.Instance, new P(0,0));
            var milliseconds = st.Elapsed.TotalMilliseconds;
            Debug.Log(milliseconds);
            
            
            //LineRenderer.DrawLineInGameView(new Vector3[] { Vector3.zero, Vector3.one}, Color.magenta);
            
            for (var i = 0; i < VisibilityCalculator.VisibleCells.Count; i++)
            {
                Lines.Draw(Color.green, new Vector3[]
                {
                    GameSettings.Instance.PositionFx(VisibilityCalculator.VisibleCells[i]),
                    GameSettings.Instance.PositionFx(VisibilityCalculator.VisibleCells[i]) + Vector3.one * 0.01f,
                });
            }
            
            for (var i = 0; i < VisibilityCalculator.NotVisibleCells.Count; i++)
            {
                Lines.Draw(Color.red, new Vector3[]
                {
                    GameSettings.Instance.PositionFx(VisibilityCalculator.NotVisibleCells[i]),
                    GameSettings.Instance.PositionFx(VisibilityCalculator.NotVisibleCells[i]) + Vector3.one * 0.01f,
                });
            }
            
            for (var i = 0; i < VisibilityCalculator.Obstacles.Count; i++)
            {
                var obstacle = VisibilityCalculator.Obstacles[i];
                Lines.Draw(Color.yellow, new Vector3[]
                {
                    new Vector3(obstacle.A.x, obstacle.A.y),
                    new Vector3(obstacle.B.x, obstacle.B.y),
                });
            }
            
            for (var i = 0; i < VisibilityCalculator.Obstacles.Count; i++)
            {
                var obstacle = VisibilityCalculator.Obstacles[i];
                Lines.DrawCloseToCenter(Color.blue, 0.1f, new Vector3[]
                {
                    new Vector3(obstacle.Points[0].x, obstacle.Points[0].y),
                    new Vector3(obstacle.Points[1].x, obstacle.Points[1].y),
                    new Vector3(obstacle.Points[2].x, obstacle.Points[2].y),
                    new Vector3(obstacle.Points[3].x, obstacle.Points[3].y),
                    new Vector3(obstacle.Points[4].x, obstacle.Points[4].y),
                    new Vector3(obstacle.Points[5].x, obstacle.Points[5].y),
                    new Vector3(obstacle.Points[0].x, obstacle.Points[0].y)
                });
            }
        }

    }
}