﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

namespace DefaultNamespace
{
    public static class Sugar
    {
        public static int Loop(this int selectedType, int min, int max)
        {
            if (selectedType < min)
            {
                selectedType = max;
            } 
            else if (selectedType > max)
            {
                selectedType = min;
            }

            return selectedType;
        }
        
        public static int Loop(this int selectedType, int add, int min, int max)
        {
            selectedType += add;
            if (selectedType < min)
            {
                selectedType = max;
            } 
            else if (selectedType > max)
            {
                selectedType = min;
            }

            return selectedType;
        }
        
        public static string[] Split (this string s, params string[] splits)
        {
            return s.Split(splits, StringSplitOptions.RemoveEmptyEntries);
        }
        
        public static List<int> ToInts (this string s, params string[] splits)
        {
            var values = s.Split(splits, StringSplitOptions.RemoveEmptyEntries);
            List<int> result = new List<int>();

            foreach (var value in values)
            {
                result.Add(int.Parse(value));
            }

            return result;
        }
        
        public static HashSet<T> ToSet<T> (this ICollection<T> collection)
        {
            var set = new HashSet<T>();

            foreach (var t in collection)
            {
                set.Add(t);
            }

            return set;
        }

        public static void AddRange<T> (this HashSet<T> set, ICollection<T> collection)
        {
            foreach (var t in collection)
            {
                set.Add(t);
            }
        }
        public static V GetOr<K,V>(this Dictionary<K,V> d, K key, V defaultV)
        {
            if (d.TryGetValue(key, out V v))
            {
                return v;
            }

            return defaultV;
        }
        
        public static V GetOrNew<K,V>(this Dictionary<K,V> d, K key) where V : new()
        {
            if (d.TryGetValue(key, out V v))
            {
                return v;
            }

            v = new V();
            d[key] = v;
            return v;
        }
        
        public static int Sum<K>(this Dictionary<K,int> d, K key, int amount) 
        {
            if (!d.TryGetValue(key, out int v))
            {
                v = 0;
            }

            d[key] = amount + v;
            return amount + v;
        }
        
        public static bool Intersects(this Rect r1, Rect r2, out Rect area)
        {
            area = new Rect();
 
            if (r2.Overlaps(r1))
            {
                float x1 = Mathf.Min(r1.xMax, r2.xMax);
                float x2 = Mathf.Max(r1.xMin, r2.xMin);
                float y1 = Mathf.Min(r1.yMax, r2.yMax);
                float y2 = Mathf.Max(r1.yMin, r2.yMin);
                area.x = Mathf.Min(x1, x2);
                area.y = Mathf.Min(y1, y2);
                area.width = Mathf.Max(0.0f, x1 - x2);
                area.height = Mathf.Max(0.0f, y1 - y2);
           
                return true;
            }
 
            return false;
        }
        
        public static double NextDouble (this Random random, double min, double max)
        {
            return random.NextDouble() * (max-min) + min;
        }
        
        public static float NextFloat (this Random random, float min, float max)
        {
            return (float)random.NextDouble(min, max);
        }

        public static bool LineLineIntersection(out Vector2 intersection, Vector2 line1Point1, Vector2 line1Point2, Vector2 line2Point1, Vector2 line2Point2)
        {
            var vec1 = line1Point2 - line1Point1;
            var vec2 = line2Point2 - line2Point1;
            if (LineLineIntersection2(out intersection, line1Point1, vec1.normalized, line2Point1, vec2.normalized))
            {
                if (PointBelongsToLine(line1Point1, line1Point2, intersection))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool PointBelongsToLine(Vector2 a, Vector2 b, Vector2 test)
        {
            var c = (a + b) / 2;
            var d = (a - b).magnitude / 2;
            var d2 = (test - c).magnitude;
            
            
            return d2 <= d;
        }
        
        private static bool LineLineIntersection2(out Vector2 intersection, Vector2 linePoint1, Vector2 lineVec1, Vector2 linePoint2, Vector2 lineVec2){

            Vector2 lineVec3 = linePoint2 - linePoint1;
            Vector2 crossVec1and2 = Vector3.Cross(lineVec1, lineVec2);
            Vector2 crossVec3and2 = Vector3.Cross(lineVec3, lineVec2);

            float planarFactor = Vector3.Dot(lineVec3, crossVec1and2);

            //is coplanar, and not parallel
            if( Mathf.Abs(planarFactor) < 0.0001f  && crossVec1and2.sqrMagnitude > 0.0001f)
            {
                float s = Vector3.Dot(crossVec3and2, crossVec1and2)  / crossVec1and2.sqrMagnitude;
                intersection = linePoint1 + (lineVec1 * s);
                return true;
            }
            else
            {
                intersection = Vector3.zero;
                return false;
            }
        }
        
        public static V RemoveReturn<K, V>(this Dictionary<K, V> d, K key)
        {
            var v = d.GetOr(key, default(V));
            d.Remove(key);
            return v;
        }
        
        public static V GetOrAdd<K,V>(this Dictionary<K,V> d, K key, Func<V> func) where V : new()
        {
            if (!d.TryGetValue(key, out V v))
            {
                v = func();
                d[key] = v;
                return v;
            }

            return v;
        }
        
        public static void RemoveAndKey<K,V>(this Dictionary<K,List<V>> d, K key, V value)
        {
            if (!d.TryGetValue(key, out List<V> v))
            {
                return;
            }

            v.Remove(value);
            if (v.Count == 0)
            {
                d.Remove(key);
            }
        }
    }
}