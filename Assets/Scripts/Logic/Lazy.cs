﻿using System;

namespace Logic
{
    public class Lazy<T>
    {
        private T _data;
        private Func<T> func;

        public Lazy(Func<T> func)
        {
            this.func = func;
        }

        public T Data
        {
            get
            {
                if (_data == null)
                {
                    _data = func();
                }

                return _data;
            }
        }
    }
}