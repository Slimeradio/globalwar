﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace DefaultNamespace
{
    [XmlRoot("Definitions")]
    public class Definitions
    {
        [XmlArrayItem("Definition")]
        public List<BaseDefinition> List = new List<BaseDefinition>();
    }
    
    public class BaseDefinition
    {
        [XmlAttribute]
        public string Name;
    }
}