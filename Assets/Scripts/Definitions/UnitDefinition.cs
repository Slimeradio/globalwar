﻿using System.Xml.Serialization;

namespace DefaultNamespace
{
    public interface IBuildable
    {
        int BuildCost { get; }
        int BuildTime { get; }
    }
    
    public class UnitDefinition : BaseDefinition, IBuildable 
    {
        [XmlElement]
        public int MaxHp = 4;
        
        [XmlElement]
        public int SpriteId;
        
        [XmlElement]
        public int MaxRestoredHp = 3;
        
        [XmlElement]
        public float MovementSpeed = 3;
        
        [XmlElement]
        public float Damage = 0.5f;
        
        [XmlElement]
        public int Range = 3;
        
        [XmlElement]
        public int ShootRate = 30;
        
        [XmlElement]
        public int ShootReload = 60;
        
        [XmlElement]
        public int Cost = 60;
        
        [XmlElement("BuildTime")]
        protected int _buildTime = 60;
        
        [XmlElement]
        public int CaptureAmount = 1;
        
        [XmlArray]
        [XmlArrayItem("Speed")]
        public int[] PerSurfaceMovementSpeed =
        {
            20,
            30,
            40,
            50,
            60,
            120,
            -1,
            -1,
            60,
            60,
            60,
            60,
            60,
            60,
            60,
            60,
        };
    
        [XmlArray]
        [XmlArrayItem("Speed")]
        public int[] PerSurfaceMovementSpeedWithRoad =
        {
            20,
            30,
            40,
            50,
            60,
            120,
            -1,
            -1,
            60,
            60,
            60,
            60,
            60,
            60,
            60,
            60,
        };

        
        
        [XmlIgnore]
        public int BuildCost => Cost;
        
        [XmlIgnore]
        public int BuildTime => _buildTime;
    }
}