﻿using System.Xml.Serialization;

namespace DefaultNamespace
{
    public class BuildingDefinition : BaseDefinition
    {
        [XmlElement]
        public int MaxHp;
        
        [XmlElement]
        public int SpriteId;
        
        [XmlElement]
        public int CaptureRadius = 1;
        
        [XmlElement]
        public int CaptureHP = 10;
    }
    
    public class EcoProducerDefinition : BuildingDefinition
    {
        [XmlArray]
        [XmlArrayItem("Value")]
        public int[] ResourceGain;
        
        [XmlArray]
        [XmlArrayItem("Value")]
        public int[] ResourceMax;
    }
}