﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using UnityEngine;

namespace DefaultNamespace
{
    public class R
    {
        private static Dictionary<string, BaseDefinition> Definitions = new Dictionary<string, BaseDefinition>();

        private static Type[] _includes;

        public static T Get<T>(string name) where T : class
        {
            var t = Definitions.GetOr(name, null) as T;
            return t;
        }
        
        private static Type[] Includes
        {
            get
            {
                if (_includes == null)
                {
                    var list = new List<Type>();
                    var assemblies = AppDomain.CurrentDomain.GetAssemblies();
                    foreach (var assembly in assemblies)
                    {
                        try
                        {
                            foreach (var type in assembly.GetTypes())
                            {
                                if (typeof(BaseDefinition).IsAssignableFrom(type))
                                {
                                    list.Add(type);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            
                        }
                    }

                    _includes = list.ToArray();
                }

                return _includes;
            }
        }
        
        static R()
        {
            
            Debug.LogWarning(Directory.GetCurrentDirectory());
            try
            {
                LoadDefinitions("Assets/Resources/data.xml");
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
        }

        public static T Load<T>(string path)
        {
            var txt = File.ReadAllText(path);
            //string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
            //if (txt.StartsWith(_byteOrderMarkUtf8))
            //{
            //    txt = txt.Remove(0, _byteOrderMarkUtf8.Length);
            //}
            
            var serializer = new XmlSerializer(typeof(T), Includes);
            using (StringReader stringReader = new StringReader(txt))
            {
                return (T)serializer.Deserialize(stringReader);
            }
        }
        
        public static List<T> LoadMany<T>(string path)
        {
            var list = new List<T>();
            var files = Directory.GetFiles(path, "*.xml");
            foreach (var file in files)
            {
                try
                {
                    var f = Load<T>(file);
                    list.Add(f);
                }
                catch (Exception e)
                {
                    
                }
            }

            return list;
        }
        
        public static void Save<T>(string path, T value)
        {
            var serializer = new XmlSerializer(typeof(T), Includes);
            var sb = new StringBuilder();
            using (StringWriter stringReader = new StringWriter(sb))
            {
                serializer.Serialize(stringReader, value);
            }
            File.WriteAllText(path, sb.ToString());
        }
        
        public static void LoadDefinitions (string path)
        {
            var definitions = Load<Definitions>(path);
            foreach (var value in definitions.List)
            {
                Definitions[value.Name] = value;
            }
        }
    }
}