﻿using System;
using System.Collections.Generic;
using DefaultNamespace;
using Logic;
using UnityEditor;
using UnityEngine;

namespace DebugHelpers
{
    public class Lines : MonoBehaviour
    {
        private static ObjectPools<LineDrawer, bool> Pools = new ObjectPools<LineDrawer, bool>(
            () => new LineDrawer(), 
            null, 
            null, 
            (x) => x.Destroy(), 
            1000);

        public static List<LineDrawer> Used = new List<LineDrawer>();

        public void Update()
        {
            foreach (var drawer in Used)
            {
                Pools.Put(drawer);
            }
            Used.Clear();
        }

        public static void Draw(Color color, Vector3[] positions)
        {
            var lineDrawer = Pools.Take(false);
            Used.Add(lineDrawer);
            lineDrawer.DrawLineInGameView(positions, color, 0.1f);
        }
        
        public static void DrawCloseToCenter(Color color, float amount, Vector3[] positions)
        {
            var center = Vector3.zero;

            foreach (var position in positions)
            {
                center += position;
            }

            center /= positions.Length;

            for (var i = 0; i < positions.Length; i++)
            {
                positions[i] = positions[i].Lerp(center, amount);
            }

            var lineDrawer = Pools.Take(false);
            Used.Add(lineDrawer);
            lineDrawer.DrawLineInGameView(positions, color, 0.1f);
        }
    }
}