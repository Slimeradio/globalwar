﻿using System;
using System.Collections.Generic;
using ANE.A;
using UnityEngine;

namespace DefaultNamespace
{
    
    public class Player
    {
        public Color Color = Color.white;
        public String name = "";
        public Resource[] Resources = new Resource[3];
        public List<Unit> Units = new List<Unit>();
        public List<Building> Buildings = new List<Building>();

        public HashSet<Cell> GetVisibleCells()
        {
            var set = new HashSet<Cell>();
            foreach (var unit in Units)
            {
                unit.GetVisibleCells(set);
            }

            return set;
        }

        public Player()
        {
            Resources[0] = new Resource();
            Resources[1] = new Resource();
            Resources[2] = new Resource();
        }
        
        public bool DrainResources(AFactory aFactory, IBuildable definition)
        {
            return true;
        }

        public void ChangeResources(Building building, int[] income, int incomeMlt, int[] max, int maxMlt)
        {
            if (income != null)
            {
                for (int i = 0; i < income.Length; i++)
                {
                    Resources[i].Income += income[i] * incomeMlt;
                }
            }

            if (max != null)
            {
                for (int i = 0; i < max.Length; i++)
                {
                    Resources[i].Income += max[i] * maxMlt;
                }
            }
            
        }
    }
}