﻿using Logic;
using UnityEngine;

namespace DefaultNamespace
{
    public class GameSettings
    {
        public static GameSettings Instance = new GameSettings();

        public float MovementBetweenCells = 3f; //3s
        public int CaptureInterval = 60; //60s
        
        public Lazy<Sprite[]> CellSprites = new Lazy<Sprite[]>(()=> Resources.LoadAll<Sprite>("Sprites/cells"));
        public Lazy<Sprite[]> UnitSprites = new Lazy<Sprite[]>(()=> Resources.LoadAll<Sprite>("Sprites/units"));
        public Lazy<Sprite[]> BuildingSprites = new Lazy<Sprite[]>(()=> Resources.LoadAll<Sprite>("Sprites/buildings"));

        
        public float DistanceX1 = 0.95f;
        public float DistanceY1 = 0f;
        public float DistanceX2 = -0.45f;
        public float DistanceY2 = 0.95f;
        
        public Vector3 PositionFx(int x, int y)
        {
            var yy = (x % 2) * DistanceX2 - (y + (x < 0 ? 1 : 0)) * DistanceY2; 
            return new Vector3(x * DistanceX1 - y * DistanceY1, yy, yy);//x+ y); // + 
        }
        
        public Vector3 PositionFx(Cell cell)
        {
            return PositionFx(cell.X, cell.Y);
        }
    }
}