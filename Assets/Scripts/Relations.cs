﻿namespace DefaultNamespace
{
    public class Relations
    {
        public static bool AreEnemies(Player p1, Player p2)
        {
            return p1 != p2;
        }
        
        public static bool AreEnemies(Unit u1, Unit u2)
        {
            return AreEnemies(u1.Owner, u2.Owner);
        }
        
        //public static int CountEnemies(List<Player> players)
        //{
        //    if (players.Count == 0)
        //    {
        //        return false;
        //    }
//
        //    if (players.Count == 0)
        //    {
        //        
        //    }
        //    
        //    return AreEnemies(u1.Owner, u2.Owner);
        //}

        public static bool AreEnemies(Unit p1, Building p2)
        {
            return AreEnemies(p1.Owner, p2.Owner);
        }
    }
}