﻿using TMPro;
using UnityEngine;

namespace DefaultNamespace
{
    public class BuildingPresenter : MonoBehaviour, IBindable<Building>
    {
        public SpriteRenderer TopImage;
        public Texture2D txt;
        public TextMeshPro Text;
        public Building building;
        private MapPresenter MapPresenter;
        
        public void Bind(Building building)
        {
            
            MapPresenter = GetComponentInParent<MapPresenter>();
            this.building = building;
            
            Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/"+txt.name);  
            TopImage.sprite = sprites[building.Definition.SpriteId];
            TopImage.color = building.Owner?.Color ?? Color.white;
        }

        public Cell GetCell()
        {
            return building.Cell;
        }

        private void Update()
        {
            if (building == null) return;
            TopImage.color = building.Owner?.Color ?? Color.white;
            if (building.CaptureTick != null)
            {
                Text.color = Color.magenta;
                Text.text = (100* building.Captured / building.Definition.CaptureHP) + "%";
            }
            else
            {
                Text.text = "";
            }
            
        }
    }
}