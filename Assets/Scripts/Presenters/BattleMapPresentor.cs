﻿using System;
using UnityEngine;

namespace DefaultNamespace
{
    public class BattleMapPresentor : MonoBehaviour
    {
        public MapPresenter Presenter;

        private void Awake()
        {
            Application.targetFrameRate = 60;
        }

        private void Update()
        {
            GameLoop.Instance.Update();
            Presenter.MouseControl();
            Map map = Presenter.Map;


            var p = map.Players[0];

            var visibleCells = p.GetVisibleCells();
            foreach (var kv in Presenter.CellPresenters)
            {
                if (visibleCells.Contains(kv.Key))
                {
                    kv.Value.SetVisible(true);
                }
                else
                {
                    kv.Value.SetVisible(false);
                }
            }
        }
    }
}