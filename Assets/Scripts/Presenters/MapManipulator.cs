﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

namespace DefaultNamespace
{
    
    public class MapManipulator : MonoBehaviour
    {
        public MapPresenter Presenter;
        public Cell SelectedCell;
        private List<CellPresenter> PrevPresenter = new List<CellPresenter>();
        public Map Map => Presenter.Map;
        public void Awake()
        {
            Presenter = gameObject.GetComponent<MapPresenter>();
        }

        private void Update()
        {
            Mouse();
        }

        
        
        private void Mouse()
        {
            foreach (var presenter in PrevPresenter)
            {
                presenter.Over(false);
            }
            PrevPresenter.Clear();


            
            
            var mouseOverCell = Presenter.GetCellUnderMouse();
            if (Input.GetMouseButtonDown((int) MouseButton.LeftMouse))
            {
                SelectedCell = mouseOverCell?.Cell;
            }
            else if (Input.GetMouseButtonUp((int) MouseButton.LeftMouse))
            {
                if (SelectedCell != null && mouseOverCell != null )
                {
                    var units = Presenter.Map.CellToUnit.GetOr(SelectedCell, null);
                    if (units != null)
                    {
                        foreach (var unit in units)
                        {
                            var path = Map.FindPath(SelectedCell, mouseOverCell.Cell, unit);
                            if (path != null && path.Count > 1)
                            {
                                var cell = Map[path[1]];
                                if (cell != null)
                                {
                                    unit.Move(cell);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if (SelectedCell != null && mouseOverCell != null)
                {
                    var units = Map.CellToUnit.GetOr(SelectedCell, null);
                    if (units != null)
                    {
                        foreach (var unit in units)
                        {
                            var path = Map.FindPath(SelectedCell, mouseOverCell.Cell, unit);
                            if (path != null)
                            {
                                foreach (var pp in path)
                                {
                                    var cell = Map[pp];
                                    if (cell != null)
                                    {
                                        var p = Presenter.CellPresenters.GetOr(cell, null);
                                        if (p != null)
                                        {
                                            p.Over(true);
                                            PrevPresenter.Add(p);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}