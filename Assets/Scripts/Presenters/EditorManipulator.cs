﻿using System;
using System.Collections.Generic;
using ANE.A;
using UnityEngine;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;

namespace DefaultNamespace
{

    class Stored
    {
        public Cell Cell;
        public int OriginalType;
    }
    
    public class EditorManipulator : MonoBehaviour
    {
        public MapPresenter Presenter;
        public Cell SelectedCell;
        private List<CellPresenter> PrevPresenter = new List<CellPresenter>();
        public Map Map => Presenter.Map;
        private const string SAVE_PATH = "Assets/Resources/MapGenerators/";
        
        public void Awake()
        {
            Presenter = gameObject.GetComponent<MapPresenter>();
        }


        private int SelectedType = 0;
        private int SelectedVariant = 0;
        private Stored PrevStored;

        private List<MapGeneratorVariant> VariantsDefinitions;

        private void Start()
        {
            
            var generators = R.LoadMany<MapGeneratorVariantsDefinition>(SAVE_PATH);
            VariantsDefinitions = new List<MapGeneratorVariant>();

            foreach (var variant in generators)
            {
                foreach (var definition in variant.Variants)
                {
                    var v = new MapGeneratorVariant();
                    v.FromDefinition(definition);
                    VariantsDefinitions.Add(v);
                }
            }
        }

        private void Update()
        {
            Highlight();
            Mouse();
        }

        private void Highlight()
        {
            foreach (var presenter in Presenter.CellPresenters)
            {
                presenter.Value.SetVisible(false);
            }
            
            for (int x = 0; x < MapGeneratorVariant.Width; x++)
            {
                for (int y = 0; y < MapGeneratorVariant.Height; y++)
                {
                    var cell = Map[x, y];
                    Presenter.CellPresenters.GetOr(cell, null)?.SetVisible(true);
                }
            }
        }
        
        Recta EditRect = new Recta(0, 0, MapGeneratorVariant.Width, MapGeneratorVariant.Height);
        
        private void Mouse()
        {
            //foreach (var presenter in PrevPresenter)
            //{
            //    presenter.Over(false);
            //}
            //PrevPresenter.Clear();

            
            
            
            if (PrevStored != null)
            {
                PrevStored.Cell.Type = PrevStored.OriginalType;
                var presenter = Presenter.CellPresenters.GetOr(PrevStored.Cell, null);
                if (presenter != null)
                {
                    presenter.Bind(PrevStored.Cell);
                }
            }
                
            var mouseOverCell = Presenter.GetCellUnderMouse();
            if (mouseOverCell == null)
            {
                return;
            }
            
            var cell = mouseOverCell.Cell;


            PrevStored = new Stored();
            PrevStored.Cell = cell;
            PrevStored.OriginalType = cell.Type;

            cell.Type = SelectedType;
            
            mouseOverCell.Bind(cell);
            
            if (Input.GetMouseButton((int) MouseButton.LeftMouse))
            {
                PrevStored = null;
            }
            
            if (Input.GetMouseButton((int) MouseButton.RightMouse))
            {
                SelectedType = PrevStored.OriginalType;
            }
            
            if (Input.mouseScrollDelta.y != 0)
            {
                

                if (EditRect.Contains(cell.X, cell.Y))
                {
                    SelectedType = SelectedType.Loop(Math.Sign(Input.mouseScrollDelta.y), 0, CellsDefinition.Instance.Cells.Length - 1);
                }
                else
                {
                    SelectedVariant = SelectedVariant.Loop(Math.Sign(Input.mouseScrollDelta.y), 0, VariantsDefinitions.Count - 1);
                    
                    int xx = cell.X / MapGeneratorVariant.Width + (cell.X < 0? -1 : 0);
                    int yy = cell.Y / MapGeneratorVariant.Height + (cell.Y < 0? -1 : 0);

                    var rect = new Recta(
                        xx * MapGeneratorVariant.Width, 
                        yy * MapGeneratorVariant.Height,
                        (xx + 1) * MapGeneratorVariant.Width-1, 
                        (yy + 1) * MapGeneratorVariant.Height-1);

                    var variant = VariantsDefinitions[SelectedVariant];
                    
                    rect.Iterate((x, y, xx, yy) =>
                    {
                        var cc = Map[x, y];
                        cc.Type = variant.Cells[xx, yy];
                        Presenter.CellPresenters.GetOr(cc, null)?.Bind(cc);
                    });
                }
                
                
            }
            
            
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.S))
            {
                if (PrevStored != null)
                {
                    cell.Type = PrevStored.OriginalType;
                }
                Save();
            }
        }
        
        private void Save()
        {
            var def = new MapGeneratorVariantsDefinition();
            def.Variants = new List<MapGeneratorVariantDefinition>();


            var v = new MapGeneratorVariant();
            for (int x = 0; x < MapGeneratorVariant.Width; x++)
            {
                for (int y = 0; y < MapGeneratorVariant.Height; y++)
                {
                    v.Cells[x, y] = Map[x, y].Type;
                }
            }

            v.ID = "export-" + Random.Range(0,10000000);
                
            def.Variants.Add(v.ToDefinition());
                
            R.Save(SAVE_PATH+ "MapGenerators_" + Random.Range(0,100000)+ ".xml", def);
        }

        
    }
}