﻿using System;
using System.Collections.Generic;
using ANE.A;
using Logic;
using UnityEngine;
using UnityEngine.UIElements;

namespace DefaultNamespace
{
    public partial class MapPresenter
    {
        public ObjectPools<CellPresenter, Cell> CellPool;
        public ObjectPools<BuildingPresenter, Building> BuildingPool;
        public ObjectPools<UnitPresenter, Unit> UnitPool;
        
        public static Vector3 UnitZ = new Vector3(0, 0, -3);
        public static Vector3 BuildingZ = new Vector3(0, 0, -1);
        
        private Recta Viewport = new Recta(1,1, -1, -1);

        public Dictionary<Cell, CellPresenter> CellPresenters = new Dictionary<Cell, CellPresenter>();
        public Dictionary<Unit, UnitPresenter> UnitPresenters = new Dictionary<Unit, UnitPresenter>();
        public Dictionary<Building, BuildingPresenter> BuildingPresenters = new Dictionary<Building, BuildingPresenter>();
        
        
        
        private void OnViewportChanged(Recta? prev, Recta neww)
        {
            
            Recta.IterateOverBoth(prev, neww, (x, y, a) =>
            {
                if (a)
                {
                    var cell = Map[x, y];
                    CellPresenters[cell] = CellPool.Take(cell);
                    
                    var units = Map.CellToUnit.GetOr(cell, null);
                    if (units != null)
                    {
                        foreach (var unit in units)
                        {
                            RegisterForDraw(unit);
                        }
                    }
                    
                    var buildings = Map.CellToBuilding.GetOr(cell, null);
                    if (buildings != null)
                    {
                        foreach (var b in buildings)
                        {
                            RegisterForDraw(b);
                        }
                    }
                }
                else
                {
                    var cell = Map[x, y];
                    var p = CellPresenters.RemoveReturn(cell);
                    if (p == null)
                    {
                        var wtf = 0;
                    }
                    
                    CellPool.Put(p);

                    var units = Map.CellToUnit.GetOr(cell, null);
                    if (units != null)
                    {
                        foreach (var unit in units)
                        {
                            UnRegisterFromDraw(unit);
                        }
                    }
                    
                    var buildings = Map.CellToBuilding.GetOr(cell, null);
                    if (buildings != null)
                    {
                        foreach (var b in buildings)
                        {
                            UnRegisterFromDraw(b);
                        }
                    }
                }
            });

            Viewport = neww;
        }
        
        private void UnRegisterFromDraw(IMapObject mapObject)
        {
            if (mapObject is Unit u)
            {
                var p = UnitPresenters.RemoveReturn(u);
                if (p != null)
                {
                    UnitPool.Put(p);
                }
            } 
            else if (mapObject is Building b)
            {
                var p = BuildingPresenters.RemoveReturn(b);
                if (b != null)
                {
                    BuildingPool.Put(p);
                }
            }
        }
        
        private void RegisterForDraw(IMapObject mapObject)
        {
            if (mapObject is Unit u)
            {
                UnitPresenters[u] = UnitPool.Take(u);
            } 
            else if (mapObject is Building b)
            {
                BuildingPresenters[b] = BuildingPool.Take(b);
            }
        }
        
        private void OnMapMoveGlobal(IMapObject arg1, Cell from, Cell to)
        {
            if (to == null || !Viewport.Contains(to.X, to.Y))
            {
                UnRegisterFromDraw(arg1);
            }
            else
            {
                RegisterForDraw(arg1);
            }
        }

        private T Instantiate2<T>(GameObject prefab) where T : MonoBehaviour
        {
            var cell = Instantiate(prefab, Vector3.zero, Quaternion.identity, holder.transform);
            return cell.GetComponentInChildren<T>();
        }
        
        private void Apply<T, V>(T presenter, V value, Vector3 offset) where T : MonoBehaviour, IBindable<V>
        {
            if (value == null)
            {
                var wtf = 0;
            }
            presenter.Bind(value);
            var cell = presenter.GetCell();
            if (cell != null)
            {
                presenter.gameObject.transform.localPosition = GameSettings.Instance.PositionFx(cell.X, cell.Y) + offset;
                presenter.gameObject.SetActive(true);
            }
        }
        
        private void CreatePools()
        {
            CellPool = new ObjectPools<CellPresenter, Cell>(
                () => Instantiate2<CellPresenter>(CellPrefab),
                presenter => presenter.gameObject.SetActive(false),
                (presenter, data) => Apply(presenter, data, Vector3.zero), 
                presenter => Destroy(presenter.gameObject), 
                99999);
            
            UnitPool = new ObjectPools<UnitPresenter, Unit>(
                () => Instantiate2<UnitPresenter>(UnitPrefab),
                presenter => presenter.gameObject.SetActive(false),
                (presenter, data) => Apply(presenter, data, UnitZ), 
                presenter => Destroy(presenter.gameObject), 
                99999);
            
            BuildingPool = new ObjectPools<BuildingPresenter, Building>(
                () => Instantiate2<BuildingPresenter>(BuildingPrefab),
                presenter => presenter.gameObject.SetActive(false),
                (presenter, data) => Apply(presenter, data, BuildingZ), 
                presenter => Destroy(presenter.gameObject), 
                99999);
        }
    }
    public partial class MapPresenter : MonoBehaviour
    {
        public Map Map;

        
        
        public GameObject CellPrefab;
        public GameObject UnitPrefab;
        public GameObject BuildingPrefab;
        public GameObject holder;

        private Vector3? lastPosition;

        public float ScrollSensitivity = 30;
        //private LineDrawer lineDrawer;
        
        public int Width;
        public int Height;

        private int MouseDelta = 0;
        public bool CanZoom = true;
        private void Start()
        {
            CreatePools();
            Map = new Map().Init();

            UpdateViewport();
            
            Map.OnMoveGlobal += OnMapMoveGlobal;
            //lineDrawer = new LineDrawer();
        }

        
        public void MouseControl()
        {
            if (CanZoom && Input.mouseScrollDelta.y != 0)
            {
                var sign = Math.Sign(Input.mouseScrollDelta.y);
                OnViewportChanged(Viewport, new Recta(Viewport.Left - sign, Viewport.Top - sign, Viewport.Right + sign, Viewport.Bottom + sign));
            }
            
            if (Input.GetMouseButton((int) MouseButton.MiddleMouse) && lastPosition != null)
            {
                var dx = lastPosition.Value - Input.mousePosition;
                var dx2 = new Vector3(dx.x / ScrollSensitivity, dx.y / ScrollSensitivity);
                gameObject.transform.localPosition -= dx2;

                UpdateViewport();
            }
            
            lastPosition = Input.mousePosition;
        }

        private void UpdateViewport()
        {
            var localPosition = gameObject.transform.localPosition;
            var rect = new Rect(-localPosition.x - Width/2, localPosition.y - Height/2, Width, Height);
                
                
            Recta r = new Recta();
            r.Left = (int) (rect.left / GameSettings.Instance.DistanceX1) - 2;
            r.Top = (int)(rect.top / GameSettings.Instance.DistanceX1) - 2;
            r.Right = r.Left + (int)(rect.width / GameSettings.Instance.DistanceX1) + 4;
            r.Bottom = r.Top + (int)(rect.height / GameSettings.Instance.DistanceX1) + 4;

            OnViewportChanged(Viewport, r);
        }
        
        public CellPresenter GetCellUnderMouse()
        {
            Vector3 mousePosition = Input.mousePosition;
            
            var toWorld = Camera.main.ScreenToWorldPoint(mousePosition);
            toWorld.z = 9999;
            Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
            RaycastHit2D hit = Physics2D.GetRayIntersection (ray, Mathf.Infinity);
            if (hit.collider != null)
            {
                Debug.DrawRay(Vector3.zero, hit.point, Color.red);
                var o = hit.collider.gameObject;
                var presenter = o.GetComponent<CellPresenter>();
                return presenter;
            }

            return null;
        }
    }
}
