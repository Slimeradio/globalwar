﻿using TMPro;
using UnityEngine;

namespace DefaultNamespace
{
    public class UnitPresenter : MonoBehaviour, IBindable<Unit>
    {
       
        public SpriteRenderer TopImage;
        public Texture2D txt;
        public TextMeshPro Text;
        public Unit unit;
        private MapPresenter MapPresenter;
        
        public void Bind(Unit unit)
        {
            MapPresenter = GetComponentInParent<MapPresenter>();
            this.unit = unit;
            Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/"+txt.name);  
            TopImage.sprite = sprites[unit.Definition.SpriteId];
            TopImage.color = (unit.Owner?.Color ?? Color.white);
        }

        public Cell GetCell()
        {
            return unit.Cell;
        }

        private void Update()
        {
            if (unit == null) return;
            Text.text = unit.HP + "/" + unit.Definition.MaxHp;

            
            if (unit.MovingAt != null)
            {
                MovementIntent atData = unit.MovingAt.data;
                var p1 = GameSettings.Instance.PositionFx(atData.from);
                var p2 = GameSettings.Instance.PositionFx(atData.to);
                Vector3 start;
                Vector3 end;
                var center = (p1 + p2) / 2;
                
                if (unit.Cell == atData.from)
                {
                    start = p1;
                    end = center;
                }
                else
                {
                    start = center;
                    end = p2;
                }

                gameObject.transform.localPosition = (end - start) * unit.MovingAt.Progress + start + MapPresenter.UnitZ;
            }
        }

    }
}