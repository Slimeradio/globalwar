﻿using TMPro;
using UnityEngine;

namespace DefaultNamespace
{

    interface IBindable<T>
    {
        void Bind(T t);
        Cell GetCell();
    }
    public class CellPresenter : MonoBehaviour, IBindable<Cell>
    {
        public Cell Cell;
        public SpriteRenderer TopImage;
        public Texture2D txt;
        public TextMeshPro Text;
        
        public void Bind(Cell mapCell)
        {
            Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/"+txt.name);  
            Cell = mapCell;
            TopImage.sprite = GameSettings.Instance.CellSprites.Data[mapCell.Type];
            Text.text = mapCell.X + " " + mapCell.Y;
            
            
            Cell = mapCell;
            TopImage.sprite = sprites[mapCell.Type];
            //Text.text = mapCell.X + " " + mapCell.Y;
            Text.text = mapCell.Type + " " + (Cell.BlocksVisibility ? "True" : "False");// mapCell.X + " " + mapCell.Y;
            //TopImage.sprite = mapCell.Type
        }

        public Cell GetCell()
        {
            return Cell;
        }

        public void Over(bool b)
        {
            TopImage.color = b ? Color.red : Color.white;
            Text.color = b ? Color.red : Color.white;
        }

        public void SetVisible(bool visible)
        {
            TopImage.color = visible ? Color.white : Color.gray;
        }
    }
}