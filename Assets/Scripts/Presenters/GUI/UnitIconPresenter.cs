﻿using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace DefaultNamespace.GUI
{

    public interface IIcon
    {
        Sprite GetIcon();
    }
    public class IconPresenter<T> : MonoBehaviour where T : IIcon
    {
        public SpriteRenderer TopImage;
        public T Data;
        public Texture2D txt;
        public virtual void Bind(T data)
        {
            this.Data = data;
            TopImage.sprite = Data.GetIcon();
        }
    }
    public class UnitIconPresenter : IconPresenter <Unit>
    {
        public TextMeshPro Text;
        
        public void Bind(Unit unit)
        {
            TopImage.color = (unit.Owner?.Color ?? Color.white);
        }
    }
}