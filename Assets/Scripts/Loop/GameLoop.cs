﻿using System;
using System.Collections.Generic;

namespace DefaultNamespace
{
    public class GameLoop
    {
        public static GameLoop _instance;
        public static GameLoop Instance
        {
            get {
                if (_instance == null)
                {
                    _instance = new GameLoop();
                }
    
                return _instance;
            }
        }
    
        public int frame;
        public Dictionary<int, List<Action>> PerFrameActions = new Dictionary<int, List<Action>>();
        public void Update()
        {
            var actions = PerFrameActions.GetOr(frame, null);
            if (actions == null)
            {
                frame++;
                return;
            }
            foreach (var action in actions)
            {
                action.Invoke();
            }
            actions.Clear();
            frame++;
        }
    
        public void Delay(int frames, Action action)
        {
            if (frames <= 0)
            {
                action();
                throw new Exception("Can't");
            }
    
            PerFrameActions.GetOrNew(frame + frames).Add(action);
        }
        
        public Cancelable<T> DelayCancellable<T>(int frames, Action<T> action, T data)
        {
            if (frames <= 0)
            {
                action(data);
                throw new Exception("Can't");
            }
    
            var c = new Cancelable<T>(action, data, frame + frames, frame);
            PerFrameActions.GetOrNew(frame + frames).Add(c.Invoke);
    
            return c;
        }
        
        public void DelayCancellable<T>(ref Cancelable<T> where, int frames, Action<T> action, T data)
        {
            if (where != null)
            {
                throw new Exception("Already launched");
            }
            if (frames <= 0)
            {
                action(data);
                throw new Exception("Can't");
            }
    
            where = new Cancelable<T>(action, data, frame + frames, frame);
            PerFrameActions.GetOrNew(frame + frames).Add(where.Invoke);
        }
        
        public Cancelable DelayCancellable(int frames, Action action)
        {
            if (frames <= 0)
            {
                action();
                throw new Exception("Can't");
            }
    
            var c = new Cancelable(action, frame + frames, frame);
            PerFrameActions.GetOrNew(frame + frames).Add(action);
    
            return c;
        }
    
        public void Remove(int atFrame, Action action)
        {
            if (atFrame == 0)
            {
                throw new Exception("Can't");
            }
            PerFrameActions.RemoveAndKey(atFrame, action);
        }
    }
}