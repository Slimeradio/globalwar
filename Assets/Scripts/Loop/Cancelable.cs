﻿using System;

namespace DefaultNamespace
{
    public class Cancelable
    {
        private Action action;
        private int atFrame;
        private int statedAt;
        
        public float Progress => (float) (GameLoop.Instance.frame - statedAt) / (atFrame - statedAt);
        public Cancelable(Action action, int atFrame, int statedAt)
        {
            this.action = action;
            this.atFrame = atFrame;
            this.statedAt = statedAt;
        }

        public void Cancel()
        {
            GameLoop.Instance.Remove(atFrame, action);
        }
    }

    public class Cancelable<T>
    {
        private Action<T> action;
        public T data;
        public int atFrame;
        public int statedAt;

        public float Progress => (float) (GameLoop.Instance.frame - statedAt) / (atFrame - statedAt);

        public Cancelable(Action<T> action, T data, int atFrame, int statedAt)
        {
            this.action = action;
            this.statedAt = statedAt;
            this.atFrame = atFrame;
            this.data = data;
        }

        public void Invoke()
        {
            object target = action.Target;
            if (target is IMapObject)
            {
                
            }
            action(data);
        }
    
        public void Cancel()
        {
            GameLoop.Instance.Remove(atFrame, Invoke);
        }
    }
}