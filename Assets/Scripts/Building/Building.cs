﻿using DefaultNamespace.GUI;
using Logic;
using UnityEngine;

namespace DefaultNamespace
{

    public abstract partial class Building : IAliveMapObject, IIcon
    {
        public int HP;

        protected BuildingDefinition BaseDefinition;
        public BuildingDefinition Definition => BaseDefinition;

        public Building(BuildingDefinition definition)
        {
            BaseDefinition = definition;
            watcher = new AreaWatcher(this, definition.CaptureRadius);
        }
        
        private Cell _cell;
        public Cell Cell
        {
            get
            {
                return _cell;
            }
            protected set
            {
                if (_cell != value)
                {
                    var old = _cell;
                    _cell = value;
                    OnCellChanged(old, _cell);
                }
            }
        }

        private void OnCellChanged(Cell old, Cell cell)
        {
            if (old != null)
            {
                Map.Instance.Unregister(old, this);
            }
            
            if (cell != null)
            {
                Map.Instance.Register(cell, this);
            }

            watcher.OnCellChanged(old, cell);
        }

        private Player _owner;
        public Player Owner
        {
            get
            {
                return _owner;
            }
            set
            {
                if (_owner != value)
                {
                    var old = _owner;
                    _owner = value;
                    OnOwnerChanged(old, value);
                }
            }
        }

        public virtual void OnAddedToMap(Cell cell)
        {
            Cell = cell;
        }

        public Player GetPlayer()
        {
            return Owner;
        }

        public virtual void OnOwnerChanged(Player prev, Player neww)
        {
            
        }

        public bool IsAlive => HP > 0;
        public Sprite GetIcon()
        {
            return GameSettings.Instance.UnitSprites.Data[Definition.SpriteId];
        }
    }
}