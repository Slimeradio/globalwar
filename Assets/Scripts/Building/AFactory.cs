﻿namespace DefaultNamespace
{
    public class AFactory : Building
    {
        private Cancelable<UnitDefinition> BuildUnitAction;

        public void Build(UnitDefinition definition)
        {
            if (Owner.DrainResources(this, definition))
            {
                BuildUnitAction = GameLoop.Instance.DelayCancellable(definition.BuildTime, BuildUnit, definition);
            }
        }

        private void BuildUnit(UnitDefinition definition)
        {
            var unit = new Unit();
            unit.Definition = definition;
            Map.Instance.Add(unit, Cell);
        }

        public AFactory(BuildingDefinition definition) : base(definition)
        {
        }
    }
}