﻿namespace DefaultNamespace
{
    public class EcoProducer : Building
    {
        private EcoProducerDefinition Definition => BaseDefinition as EcoProducerDefinition;

        public EcoProducer(BuildingDefinition definition) : base(definition)
        {
        }
        
        public override void OnOwnerChanged(Player prev, Player neww)
        {
            base.OnOwnerChanged(prev, neww);
            if (prev != null)
            {
                prev.ChangeResources(this, Definition.ResourceGain,-1,  Definition.ResourceMax,-1);
            }

            if (neww != null)
            {
                neww.ChangeResources(this, Definition.ResourceGain,1,  Definition.ResourceMax,1);
            }
        }

        
    }
}