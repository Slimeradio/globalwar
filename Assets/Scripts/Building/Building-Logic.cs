﻿using System.Collections.Generic;
using System.Linq;
using Logic;

namespace DefaultNamespace
{
    public abstract partial class Building
    {
        private AreaWatcher watcher;
        public Cancelable<Player> CaptureTick;
        public int Captured;

        public void StartCapture(Player by)
        {
            if (CaptureTick == null)
            {
                GameLoop.Instance.DelayCancellable(ref CaptureTick, GameSettings.Instance.CaptureInterval, CaptureTickAction, by);
            }
        }

        public void CaptureTickAction(Player by)
        {
            CaptureTick = null;
            var amount = GetCaptureAmount(by);
            if (amount == 0)
            {
                OnCaptureStop();
                return;
            }
            
            Captured += amount;
            if (Captured >= Definition.CaptureHP)
            {
                Captured = 0;
                CaptureTick = null;
                Owner = by;
            }
            else
            {
                GameLoop.Instance.DelayCancellable(ref CaptureTick, GameSettings.Instance.CaptureInterval, CaptureTickAction, by);
            }
        }

        private void OnCaptureStop()
        {
            Captured = 0;
            CaptureTick = null;
        }

        public int GetCaptureAmount(Player captureBy)
        {
            
            if (watcher.UnitsNear.Count == 0)
            {
                return 0;
            } 
            
            int tickAmount = 0;
            foreach (var u in watcher.UnitsNear)
            {
                if (captureBy == u.Owner && captureBy != null)
                {
                    tickAmount += u.Definition.CaptureAmount;
                }
                else
                {
                    return 0;
                }
            }
            return tickAmount;
        }
    }
}