﻿namespace DefaultNamespace
{
    public static class Builders
    {
        public static Unit BuildUnit(UnitDefinition definition, Cell cell, Player p)
        {
            var u1 = new Unit();
            u1.Definition = definition;
            u1.HP = u1.Definition.MaxHp;
            u1.Owner = p;
            p.Units.Add(u1);
            Map.Instance.Add(u1, cell);
            return u1;
        }
        
        public static Building BuildBuilding(BuildingDefinition definition, Cell cell, Player p)
        {
            var u1 = new EcoProducer(definition);
            u1.HP = u1.Definition.MaxHp;
            u1.Owner = p;
            if (p != null)
            {
                p.Buildings.Add(u1);
            }
            Map.Instance.Add(u1, cell);
            return u1;
        }
    }
}