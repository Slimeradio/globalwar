﻿using System;

namespace DefaultNamespace
{
    public enum ResourceNames
    {
        Food = 0,
        Metal = 1,
        Oil = 2
    }
    
    public enum ResourceType
    {
        FOOD = 0,
        METAL = 1,
        FUEL = 2
    }
    
    public class Resource
    {
        private static int INTERVAL = 60;
        private int _max;
        private int _income;
        private int _lastCurrent;
        private int _lastUpdate;
        
        public int Income
        {
            get
            {
                return _income;
            }
            set
            {
                if (_income != value)
                {
                    Recalculate();
                    _income = value;
                }
            }
        }
        
        public int Max
        {
            get
            {
                return _max;
            }
            set
            {
                if (_max != value)
                {
                    _max = value;
                    Recalculate();
                }
            }
        }



        
        
        public int Current
        {
            get
            {
                var timeSpan = (GameLoop.Instance.frame - _lastUpdate) / INTERVAL;
                var amount = Math.Min(Max, _lastCurrent + timeSpan * Income);
                return amount;
            }
            set
            {
                if (value < 0)
                {
                    throw new Exception("Less than zero");
                }
                _lastCurrent = Math.Min(value, Max);
            }
        }
        
        private void Recalculate()
        {
            Current = Current;
        }
    } 
}