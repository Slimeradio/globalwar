﻿using System;
using System.Collections.Generic;
using Logic;

namespace DefaultNamespace
{
    public partial class Unit : IAliveMapObject
    {
        public float HP;
        public UnitDefinition Definition = new UnitDefinition();
    
        private Cell _cell;
        private AreaWatcher watcher;
        public Cell Cell
        {
            get
            {
                return _cell;
            }
            protected set
            {
                Map.Instance.Move(_cell, value, this);
                watcher.OnCellChanged(_cell, value);
                _cell = value;
            }
        }
        
        public Cell DestinationCell;
        public Cell FromCell;
        public Player Owner;
    
        public int GetMoveTime(Cell cell)
        {
            return Definition.PerSurfaceMovementSpeed[cell.Type];
        }

        public int CalculateETA(List<Cell> cells)
        {
            if (cells == null) throw new Exception("Cells are null");
            if (cells.Count < 2) throw new Exception($"Wrong cells count {cells.Count}");

            int time = 0;
            for (int i = 1; i < cells.Count; i++)
            {
                time += GetMoveTime(cells[i - 1]);
                time += GetMoveTime(cells[i]);
            }

            return time;
        }
    
        public void OnAdded()
        {
            GameLoop.Instance.Delay(1, OnInit);
        }

        public bool IsEnemyTo(Unit unit)
        {
            return Owner != unit.Owner;
        }


        public bool IsAlive => HP > 0;

        public void GetVisibleCells(HashSet<Cell> buffer)
        {
            var notVisible = _cell.GetNotVisibleCells();
            var radius = Map.Instance.GetCellsInRadius(Definition.Range, _cell.X, _cell.Y);
            var radius1 = Map.Instance.GetCellsInRadius(0, _cell.X, _cell.Y); //Close is always visible

            foreach (var cell in notVisible)
            {
                radius.Remove(cell);
            }

            buffer.AddRange(radius);
            buffer.AddRange(radius1);
        }
    }

    
    
    
}

