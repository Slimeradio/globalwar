﻿using System;
using System.Collections.Generic;
using DefaultNamespace.GUI;
using Logic;
using UnityEngine;

namespace DefaultNamespace
{
    public class MovementIntent
    {
        public Cell from;
        public Cell to;

        public MovementIntent(Cell from, Cell to)
        {
            this.from = from;
            this.to = to;
        }
    }
    public partial class Unit : IIcon
    {
        public Cancelable<Unit> ShootingAt;
        public Cancelable<Unit> Reloading;
        public Cancelable<MovementIntent> MovingAt;

        public void OnInit()
        {
            AutomaticLogic();
        }

        public Unit()
        {
            this.watcher = new AreaWatcher(this, Definition.Range);
            watcher.OnChanged += WatcherOnOnChanged;
        }

        private void WatcherOnOnChanged()
        {
            AutomaticLogic();
        }

        public void AutomaticLogic()
        {
            if (ShootingAt != null || Reloading != null)
            {
                return;
            }
            
            var units = watcher.UnitsNear;
            Unit enemy = null;
            foreach (var unit in units)
            {
                if (unit.IsEnemyTo(this))
                {
                    enemy = unit;
                    break;
                }
            }
            

            if (enemy != null)
            {
                StartShootAt(enemy);
            }
        }

        public void Move(Cell where)
        {
            if (where == null)
            {
                return;
            }

            if (!Map.Instance.IsInRange(where, Cell, 1) && Cell != where)
            {
                throw new Exception($"Not near! {where} {Cell}");
                return;
            }

            var halfMoveSpeed = GetMoveTime(Cell);
            var intent = new MovementIntent(_cell, where);
            MovingAt = GameLoop.Instance.DelayCancellable(halfMoveSpeed, Move1Halfway, intent);
        }

        private void Move1Halfway(MovementIntent where)
        {
            Cell = where.to;
            var halfMoveSpeed = GetMoveTime(Cell);
            MovingAt = GameLoop.Instance.DelayCancellable(halfMoveSpeed, Move2Complete, where);
        }
        
        private void Move2Complete(MovementIntent ignored)
        {
            MovingAt = null;
        }
        
            
        public void StartShootAt(Unit enemy)
        {
            ShootingAt = GameLoop.Instance.DelayCancellable(Definition.ShootRate, DoShootAt, enemy);
        }
        
        private void DoShootAt(Unit enemy)
        {
            ShootingAt = null;
            enemy.HP -= Definition.Damage;
            Reloading = GameLoop.Instance.DelayCancellable(Definition.ShootRate, ReloadingComplete, enemy);
        }
            
        private void ReloadingComplete(Unit enemy)
        {
            Reloading = null;
            AutomaticLogic();
        }

        public void OnAddedToMap(Cell cell)
        {
            Cell = cell;
        }

        public Player GetPlayer()
        {
            return Owner;
        }

        public Sprite GetIcon()
        {
            return GameSettings.Instance.UnitSprites.Data[Definition.SpriteId];
        }
    }
}